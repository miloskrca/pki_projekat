﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Collections;

namespace PKIprojekat
{
    class DBManager
    {

        private static string connectionString = "Data Source=.\\SQLEXPRESS;AttachDbFilename=E:\\Projects\\PKIprojekat\\Database.mdf;Integrated Security=True;User Instance=True";
        private static string TableRole = "tbl_role";

        public static bool CheckLogin(string uname, string pass)
        {
            System.Diagnostics.Debug.WriteLine("***Username: "+uname + " Password: " + pass);

            //if (uname.Equals("") && pass.Equals("")) return true;

            bool ret = false;
            SqlConnection sqlConn = null;
            SqlDataReader reader = null;
            try
            {
                sqlConn = new SqlConnection(connectionString);
                string queryString = "SELECT * FROM " + UserDetailsForm.TABLE_NAME
                    + " WHERE " + UserDetailsForm.COL_USERNAME + "='" + uname + "'"
                    + " AND " + UserDetailsForm.COL_PASSWORD + "='" + pass + "'";
                System.Diagnostics.Debug.WriteLine(queryString);
                SqlCommand command = new SqlCommand(queryString, sqlConn);
                sqlConn.Open();
                reader = command.ExecuteReader();
                
                ret = reader.HasRows;
                
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e);
            }
            finally
            {
                if (sqlConn != null) sqlConn.Close();
                if (reader != null) reader.Close();
            }
            return ret;
        }

        public static void ManageTabs(string uname, WindowForm form)
        {
            DataTable table = DBManager.executeQueryGet("SELECT * FROM " + UserDetailsForm.TABLE_NAME + " WHERE " + UserDetailsForm.COL_USERNAME + "='" + uname + "'");
            System.Diagnostics.Debug.WriteLine("***Count: " + table.Rows.Count);
            if (table.Rows.Count == 1)
            {
                DataRow row = table.Rows[0];

                form.DataId = row[UserDetailsForm.COL_ID].ToString();
                form.DataUsername = row[UserDetailsForm.COL_USERNAME].ToString();
                form.DataName = row[UserDetailsForm.COL_FIRSTNAME].ToString();
                form.DataLastname = row[UserDetailsForm.COL_LASTNAME].ToString();
                DBManager.ImportInCombo(form.UserDataAgency, AgencyDetailsForm.TABLE_NAME, AgencyDetailsForm.COL_NAME, AgencyDetailsForm.COL_ID);
                form.DataAgency = int.Parse(row[UserDetailsForm.COL_AGENCY].ToString());
                string role = row[UserDetailsForm.COL_ROLE].ToString();
                form.role = int.Parse(role);
                form.user_id = int.Parse(row[UserDetailsForm.COL_ID].ToString());
                switch (role)
                {
                    case "1"://sys
                        break;
                    case "2"://mod
                        form.removeTab(1);
                        form.removeTab(1);
                        break;
                    case "3"://agent
                        form.removeTab(1);
                        form.removeTab(1);
                        form.RealestateCreate.Visible = false;
                        break;
                }
                form.EnableUsername(false);
            }
        }

        public static DataTable executeQueryGet(string query)
        {
            System.Diagnostics.Debug.WriteLine("***Query: " + query);
            DataTable t = new DataTable();
            SqlConnection c = new SqlConnection(connectionString);
            try
            {
                using (c)
                {
                    c.Open();
                    // 2
                    // Create new DataAdapter
                    using (SqlDataAdapter a = new SqlDataAdapter(query, c))
                    {
                        // 3
                        // Use DataAdapter to fill DataTable
                        a.Fill(t);

                        return t;
                        // Render data onto the screen
                        // dataGridView1.DataSource = t; // <-- From your designer
                    }

                }
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e);
            }
            finally
            {
                c.Close();
            }
            return t;
        }

        public static void executeQueryInsert(string table, Hashtable hash)
        {
            SqlConnection sqlConn = new SqlConnection(connectionString);
            try
            {
                using (sqlConn)
                {
                    SqlCommand sqlComm = new SqlCommand();
                    sqlComm = sqlConn.CreateCommand();
                    string str = @"INSERT INTO " + table + " (";

                    foreach (DictionaryEntry entry in hash)
                    {
                        str += entry.Key + ", ";
                    }
                    str = str.Substring(0, str.Length - 2);
                    str += ") VALUES (";
                    foreach (DictionaryEntry entry in hash)
                    {
                        str += "@"+entry.Key + ", ";
                    }
                    str = str.Substring(0, str.Length - 2);
                    str += ")";
                    sqlComm.CommandText = str;
                    foreach (DictionaryEntry entry in hash)
                    {
                        sqlComm.Parameters.Add("@" + entry.Key, SqlDbType.VarChar);
                        sqlComm.Parameters["@"+entry.Key].Value = entry.Value;
                    }

                    sqlConn.Open();
                    sqlComm.ExecuteNonQuery();
                    sqlConn.Close();
                }
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e);
            }
        }

        public static void executeQueryUpdate(string table, Hashtable hash, string whereKey, string whereValue)
        {
            SqlConnection sqlConn = new SqlConnection(connectionString);
            try
            {
                using (sqlConn)
                {
                    SqlCommand sqlComm = new SqlCommand();
                    sqlComm = sqlConn.CreateCommand();
                    string str = @"UPDATE " + table + " SET ";

                    foreach (DictionaryEntry entry in hash)
                    {
                        str += entry.Key + "=" + "@" + entry.Key+", ";
                    }
                    str = str.Substring(0, str.Length - 2);
                    str += " WHERE " + whereKey + "=" + whereValue;
                    Console.WriteLine(str);
                    sqlComm.CommandText = str;
                    foreach (DictionaryEntry entry in hash)
                    {
                        sqlComm.Parameters.Add("@" + entry.Key, SqlDbType.VarChar);
                        sqlComm.Parameters["@" + entry.Key].Value = entry.Value;
                    }

                    sqlConn.Open();
                    sqlComm.ExecuteNonQuery();
                    sqlConn.Close();
                }
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e);
            }
        }

        public static void executeQueryDelete(string table, string whereKey, string whereValue)
        {
            SqlConnection sqlConn = new SqlConnection(connectionString);
            try
            {
                using (sqlConn)
                {
                    SqlCommand sqlComm = new SqlCommand();
                    sqlComm = sqlConn.CreateCommand();
                    string str = @"DELETE FROM " + table;
                    str += " WHERE " + whereKey + "=" + whereValue;
                    sqlComm.CommandText = str;

                    sqlConn.Open();
                    sqlComm.ExecuteNonQuery();
                    sqlConn.Close();
                }
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e);
            }
        }

        public static void ImportInCombo(System.Windows.Forms.ComboBox c, string table, string display, string value)
        {
            DataTable dt = new DataTable();

            String query = "SELECT " + display + ", "
                + value + " FROM " + table;
            SqlDataAdapter da = new SqlDataAdapter(query, connectionString);
            da.Fill(dt);

            c.DataSource = null;
            c.DataSource = dt;
            c.DisplayMember = display;
            c.ValueMember = value;
            c.SelectedValue = 0;
        }

        public static DataTable getUsersFromDatabase(Hashtable hash)
        {
            DataTable dt = new DataTable();
            string WhereParameters = null;
            bool has_params = false;
            if (hash.Count > 0)
            {
                WhereParameters = "";// " AND ";
                foreach (System.Collections.DictionaryEntry pair in hash)
                {
                    //if (!pair.Value.Equals(""))
                    //{
                        has_params = true;
                        WhereParameters += pair.Key + " LIKE '%" + pair.Value + "%' AND ";
                    //}
                }
                if (has_params)
                {
                    WhereParameters = WhereParameters.Substring(0, WhereParameters.Length - 5);
                }
            }

            string user_table = UserDetailsForm.TABLE_NAME + ".";
            string role_table = TableRole + ".";
            string column_role_id = "id_role";
            string column_name = "name";
            string agency_table = AgencyDetailsForm.TABLE_NAME + ".";

            string user_id = user_table + UserDetailsForm.COL_ID;
            string username = user_table + UserDetailsForm.COL_USERNAME;
            string user_agency = user_table + UserDetailsForm.COL_AGENCY;
            string agency_id = agency_table + AgencyDetailsForm.COL_ID;
            string agency = agency_table + column_name;
            string role = role_table + column_name;
            string role_id = role_table + column_role_id;
            string user_role = user_table + UserDetailsForm.COL_ROLE;
            string name = user_table + UserDetailsForm.COL_FIRSTNAME;
            string lastname = user_table + UserDetailsForm.COL_LASTNAME;

            String query = "SELECT "
                + user_id + ", "
                + username + ", "
                + name + ", "
                + lastname //+ ", "
                //+ agency + ", "
                //+ role
                + " FROM " + UserDetailsForm.TABLE_NAME/* + ", " + AgencyDetailsForm.TABLE_NAME*//* + ", " + TableRole*/
                + " WHERE " +/* user_role + "=" + role_id + " AND " + user_agency + "=" + agency_id +*/ (has_params ? WhereParameters : "");
            System.Diagnostics.Debug.WriteLine("***Query: " + query);
            SqlDataAdapter da = new SqlDataAdapter(query, connectionString);
            da.Fill(dt);

            dt.Columns[0].ColumnName = CurrentForm.form.ResourceManager.GetString("id");
            dt.Columns[1].ColumnName = CurrentForm.form.ResourceManager.GetString("username");
            dt.Columns[2].ColumnName = CurrentForm.form.ResourceManager.GetString("name");
            dt.Columns[3].ColumnName = CurrentForm.form.ResourceManager.GetString("lastname");
            //dt.Columns[4].ColumnName = CurrentForm.form.ResourceManager.GetString("agency");
            //dt.Columns[5].ColumnName = CurrentForm.form.ResourceManager.GetString("role");

            return dt;
        }

        public static DataTable getAgenciesFromDatabase(Hashtable hash)
        {
            DataTable dt = new DataTable();
            string WhereParameters = null;
            bool has_params = false;
            if (hash.Count > 0)
            {
                WhereParameters = " WHERE ";
                foreach (System.Collections.DictionaryEntry pair in hash)
                {
                    if (!pair.Value.Equals(""))
                    {
                        has_params = true;
                        WhereParameters += pair.Key + " LIKE '%" + pair.Value + "%' AND ";
                    }
                }
                if (has_params)
                {
                    WhereParameters = WhereParameters.Substring(0, WhereParameters.Length - 5);
                }
            }
            String query = "SELECT * FROM " + AgencyDetailsForm.TABLE_NAME + (has_params ? WhereParameters : "");
            System.Diagnostics.Debug.WriteLine("***Query: " + query);
            SqlDataAdapter da = new SqlDataAdapter(query, connectionString);
            da.Fill(dt);

            dt.Columns[0].ColumnName = CurrentForm.form.ResourceManager.GetString("id");
            dt.Columns[1].ColumnName = CurrentForm.form.ResourceManager.GetString("name");
            dt.Columns[2].ColumnName = CurrentForm.form.ResourceManager.GetString("address");
            dt.Columns[3].ColumnName = CurrentForm.form.ResourceManager.GetString("email");
            dt.Columns[4].ColumnName = CurrentForm.form.ResourceManager.GetString("phone");

            return dt;
        }

        public static DataTable getRealestatesFromDatabase(Hashtable hash)
        {
            DataTable dt = new DataTable();
            string WhereParameters = null;
            bool has_params = false;
            if (hash.Count > 0)
            {
                WhereParameters = "";
                foreach (System.Collections.DictionaryEntry pair in hash)
                {
                    switch ((string)pair.Key)
                    {
                        case RealestateDetailsForm.COL_STRUCTURE:
                        case RealestateDetailsForm.COL_AGENCY:
                        case RealestateDetailsForm.COL_AGENT:
                        case RealestateDetailsForm.COL_STATUS:
                        case RealestateDetailsForm.COL_SALE_TYPE:
                        case RealestateDetailsForm.COL_TYPE:
                            if (pair.Value != null)
                            {
                                has_params = true;
                                WhereParameters += " AND " + pair.Key + "='" + pair.Value + "'";
                            }
                            break;
                        case RealestateDetailsForm.COL_PRICE + "_from":
                            if (!pair.Value.Equals(""))
                            {
                                has_params = true;
                                WhereParameters += " AND " + RealestateDetailsForm.COL_PRICE + ">='" + pair.Value + "'";
                            }
                            break;
                        case RealestateDetailsForm.COL_PRICE + "_to":
                            if (!pair.Value.Equals(""))
                            {
                                has_params = true;
                                WhereParameters += " AND " + RealestateDetailsForm.COL_PRICE + "<='" + pair.Value + "'";
                            }
                            break;
                        case RealestateDetailsForm.COL_SURFACE + "_from":
                            if (!pair.Value.Equals(""))
                            {
                                has_params = true;
                                WhereParameters += " AND " + RealestateDetailsForm.COL_SURFACE + ">='" + pair.Value + "'";
                            }
                            break;
                        case RealestateDetailsForm.COL_SURFACE + "_to":
                            if (!pair.Value.Equals(""))
                            {
                                has_params = true;
                                WhereParameters += " AND " + RealestateDetailsForm.COL_SURFACE + "<='" + pair.Value + "'";
                            }
                            break;
                        case RealestateDetailsForm.COL_FLOOR + "_from":
                            if (!pair.Value.Equals(""))
                            {
                                has_params = true;
                                WhereParameters += " AND " + RealestateDetailsForm.COL_FLOOR + ">='" + pair.Value + "'";
                            }
                            break;
                        case RealestateDetailsForm.COL_FLOOR + "_to":
                            if (!pair.Value.Equals(""))
                            {
                                has_params = true;
                                WhereParameters += " AND " + RealestateDetailsForm.COL_FLOOR + "<='" + pair.Value + "'";
                            }
                            break;
                        case RealestateDetailsForm.COL_TERAS:
                                has_params = true;
                                WhereParameters += " AND " + RealestateDetailsForm.COL_TERAS + "='" + ((bool)pair.Value ? "1" : "0" ) + "'";
                            break;
                        case RealestateDetailsForm.COL_BOOKED:
                            has_params = true;
                            WhereParameters += " AND " + RealestateDetailsForm.COL_BOOKED + "='" + ((bool)pair.Value ? "1" : "0") + "'";
                            break;
                    }
                }
            }

            string col_list = RealestateDetailsForm.TABLE_NAME + "." + RealestateDetailsForm.COL_ID + ", "
                + AgencyDetailsForm.TABLE_NAME + "." + AgencyDetailsForm.COL_NAME + ", "
                + UserDetailsForm.TABLE_NAME + "." + UserDetailsForm.COL_USERNAME + ", "
                //+ "tbl_realestate_status.name" + ", "
                //+ "tbl_realestate_type.name" + ", "
                + RealestateDetailsForm.TABLE_NAME + "." + RealestateDetailsForm.COL_BOOKED;

            string query = "SELECT " + col_list 
                + " FROM "
                + RealestateDetailsForm.TABLE_NAME + ", " + AgencyDetailsForm.TABLE_NAME + ", " + UserDetailsForm.TABLE_NAME //+ ", tbl_realestate_status, tbl_realestate_type"
                + " WHERE "
                + RealestateDetailsForm.TABLE_NAME + ".agency=" + AgencyDetailsForm.TABLE_NAME + "." + AgencyDetailsForm.COL_ID
                + " AND " + RealestateDetailsForm.TABLE_NAME + ".agent=" + UserDetailsForm.TABLE_NAME + "." + UserDetailsForm.COL_ID
                + (has_params ? WhereParameters : ""); 
            
            //+" AND tbl_realestate_status.id_realestate_status=" + RealestateDetailsForm.TABLE_NAME + ".status"
                // + " AND tbl_realestate_type.id_realestate_type=" + RealestateDetailsForm.TABLE_NAME + ".type" +

            System.Diagnostics.Debug.WriteLine("***Query: " + query);
            SqlDataAdapter da = new SqlDataAdapter(query, connectionString);
            da.Fill(dt);

            dt.Columns[0].ColumnName = CurrentForm.form.ResourceManager.GetString("id");
            dt.Columns[1].ColumnName = CurrentForm.form.ResourceManager.GetString("agency");
            dt.Columns[2].ColumnName = CurrentForm.form.ResourceManager.GetString("agent");
            //dt.Columns[3].ColumnName = CurrentForm.form.ResourceManager.GetString("type");
            dt.Columns[3].ColumnName = CurrentForm.form.ResourceManager.GetString("booked");

            return dt;
        }

        public static void setSaleType(System.Windows.Forms.ComboBox box)
        {
            List<GenericItem> list = new List<GenericItem>();
            list.Add(new GenericItem(CurrentForm.form.ResourceManager.GetString("st_sale"), 1));
            list.Add(new GenericItem(CurrentForm.form.ResourceManager.GetString("st_rent"), 2));
            list.Add(new GenericItem(CurrentForm.form.ResourceManager.GetString("st_square_meter_sale"), 3));
            list.Add(new GenericItem(CurrentForm.form.ResourceManager.GetString("st_square_meter_rent"), 4));
            box.DataSource = list;
            box.DisplayMember = "Name";
            box.ValueMember = "Id";
            box.SelectedValue = 0;
        }

        public static void setHeating(System.Windows.Forms.ComboBox box)
        {
            List<GenericItem> list = new List<GenericItem>();
            list.Add(new GenericItem(CurrentForm.form.ResourceManager.GetString("ch"), 1));
            box.DataSource = list;
            box.DisplayMember = "Name";
            box.ValueMember = "Id";
            box.SelectedValue = 0;
        }

        public static void setStructure(System.Windows.Forms.ComboBox box)
        {
            List<GenericItem> list = new List<GenericItem>();
            list.Add(new GenericItem(CurrentForm.form.ResourceManager.GetString("one_room"), 1));
            list.Add(new GenericItem(CurrentForm.form.ResourceManager.GetString("two_rooms"), 2));
            list.Add(new GenericItem(CurrentForm.form.ResourceManager.GetString("three_rooms"), 3));
            list.Add(new GenericItem(CurrentForm.form.ResourceManager.GetString("four_rooms"), 4));
            list.Add(new GenericItem(CurrentForm.form.ResourceManager.GetString("five_rooms"), 5));
            box.DataSource = list;
            box.DisplayMember = "Name";
            box.ValueMember = "Id";
            box.SelectedValue = 0;
        }

        public static void setType(System.Windows.Forms.ComboBox box)
        {
            List<GenericItem> list = new List<GenericItem>();
            list.Add(new GenericItem(CurrentForm.form.ResourceManager.GetString("type_business"), 1));
            list.Add(new GenericItem(CurrentForm.form.ResourceManager.GetString("type_flat"), 2));
            list.Add(new GenericItem(CurrentForm.form.ResourceManager.GetString("type_garage"), 3));
            box.DataSource = list;
            box.DisplayMember = "Name";
            box.ValueMember = "Id";
            box.SelectedValue = 0;
        }

        public static void setStatus(System.Windows.Forms.ComboBox box)
        {
            List<GenericItem> list = new List<GenericItem>();
            list.Add(new GenericItem(CurrentForm.form.ResourceManager.GetString("in_construction"), 1));
            list.Add(new GenericItem(CurrentForm.form.ResourceManager.GetString("available_for_moving_in"), 2));
            box.DataSource = list;
            box.DisplayMember = "Name";
            box.ValueMember = "Id";
            box.SelectedValue = 0;
        }

        public static void setRole(System.Windows.Forms.ComboBox box)
        {
            List<GenericItem> list = new List<GenericItem>();
            list.Add(new GenericItem(CurrentForm.form.ResourceManager.GetString("role_sys_admin"), 1));
            list.Add(new GenericItem(CurrentForm.form.ResourceManager.GetString("role_moderator"), 2));
            list.Add(new GenericItem(CurrentForm.form.ResourceManager.GetString("role_agent"), 3));
            box.DataSource = list;
            box.DisplayMember = "Name";
            box.ValueMember = "Id";
            box.SelectedValue = 0;
        }

        public static void insertActivity(Activity activity)
        {
            SqlConnection sqlConn = new SqlConnection(connectionString);

            SqlCommand sqlComm = new SqlCommand();
            string sqlIns = "INSERT INTO tbl_activity (date, agent, client, remark, realestate) VALUES (@date, @agent, @client, @remark, @realestate)";
            sqlConn.Open();
            try
            {
                SqlCommand cmdIns = new SqlCommand(sqlIns, sqlConn);
                cmdIns.Parameters.Add("@date", activity.date);
                cmdIns.Parameters.Add("@agent", activity.agent);
                cmdIns.Parameters.Add("@client", activity.client);
                cmdIns.Parameters.Add("@remark", activity.remark);
                cmdIns.Parameters.Add("@realestate", activity.realestate);
                cmdIns.ExecuteNonQuery();

                cmdIns.Parameters.Clear();
                cmdIns.CommandText = "SELECT @@IDENTITY";

                // Get the last inserted id.
                int insertID = Convert.ToInt32(cmdIns.ExecuteScalar());

                cmdIns.Dispose();
                cmdIns = null;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString(), ex);
            }
            finally
            {
                sqlConn.Close();
            }
        }

        public static DataTable getActivities(int realestate, bool same)
        {
            DataTable dt = new DataTable();
            String query = "SELECT id, date, agent, " + (same ? "client, " : "") + "remark FROM tbl_activity WHERE realestate='" + realestate + "'";
            System.Diagnostics.Debug.WriteLine("***Query: " + query);
            SqlDataAdapter da = new SqlDataAdapter(query, connectionString);
            da.Fill(dt);

            dt.Columns[0].ColumnName = "Id";
            dt.Columns[1].ColumnName = "Datum";
            dt.Columns[2].ColumnName = "Agent";
            if (same)
            {
                dt.Columns[3].ColumnName = "Klijent";
                dt.Columns[4].ColumnName = "Napomena";
            }
            else
            {
                dt.Columns[3].ColumnName = "Napomena";
            }

            return dt;
        }

        public static void insertDocument(Document doc)
        {
            SqlConnection sqlConn = new SqlConnection(connectionString);

            SqlCommand sqlComm = new SqlCommand();
            string sqlIns = "INSERT INTO tbl_document (date, type, remark, realestate) VALUES (@date, @type, @remark, @realestate)";
            sqlConn.Open();
            try
            {
                SqlCommand cmdIns = new SqlCommand(sqlIns, sqlConn);
                cmdIns.Parameters.Add("@date", doc.date);
                cmdIns.Parameters.Add("@type", doc.type);
                cmdIns.Parameters.Add("@remark", doc.remark);
                cmdIns.Parameters.Add("@realestate", doc.realestate);
                cmdIns.ExecuteNonQuery();

                cmdIns.Parameters.Clear();
                cmdIns.CommandText = "SELECT @@IDENTITY";

                // Get the last inserted id.
                int insertID = Convert.ToInt32(cmdIns.ExecuteScalar());

                cmdIns.Dispose();
                cmdIns = null;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString(), ex);
            }
            finally
            {
                sqlConn.Close();
            }
        }

        public static DataTable getDocuments(int realestate)
        {
            DataTable dt = new DataTable();
            String query = "SELECT date, type, remark FROM tbl_document WHERE realestate='" + realestate + "'";
            System.Diagnostics.Debug.WriteLine("***Query: " + query);
            SqlDataAdapter da = new SqlDataAdapter(query, connectionString);
            da.Fill(dt);

            dt.Columns[0].ColumnName = "Datum";
            dt.Columns[1].ColumnName = "Tip";
            dt.Columns[2].ColumnName = "Napomena";

            return dt;
        }

        public static void insertPhoto(Photo doc)
        {
            SqlConnection sqlConn = new SqlConnection(connectionString);

            SqlCommand sqlComm = new SqlCommand();
            string sqlIns = "INSERT INTO tbl_photo (date, type, realestate) VALUES (@date, @type, @realestate)";
            sqlConn.Open();
            try
            {
                SqlCommand cmdIns = new SqlCommand(sqlIns, sqlConn);
                cmdIns.Parameters.Add("@date", doc.date);
                cmdIns.Parameters.Add("@type", doc.type);
                cmdIns.Parameters.Add("@realestate", doc.realestate);
                cmdIns.ExecuteNonQuery();

                cmdIns.Parameters.Clear();
                cmdIns.CommandText = "SELECT @@IDENTITY";

                // Get the last inserted id.
                int insertID = Convert.ToInt32(cmdIns.ExecuteScalar());

                cmdIns.Dispose();
                cmdIns = null;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString(), ex);
            }
            finally
            {
                sqlConn.Close();
            }
        }

        public static DataTable getPhotos(int realestate)
        {
            DataTable dt = new DataTable();
            String query = "SELECT date, type FROM tbl_photo WHERE realestate='" + realestate + "'";
            System.Diagnostics.Debug.WriteLine("***Query: " + query);
            SqlDataAdapter da = new SqlDataAdapter(query, connectionString);
            da.Fill(dt);

            dt.Columns[0].ColumnName = "Datum";
            dt.Columns[1].ColumnName = "Tip";

            return dt;
        }
    }
}
