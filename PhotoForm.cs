﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace PKIprojekat
{
    public partial class PhotoForm : Form
    {
        public int realeastate;
        public RealestateDetailsForm form;

        public PhotoForm()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Photo p = new Photo(
                realeastate,
                this.dateTimePicker1.Text,
                this.textBox1.Text);
            DBManager.insertPhoto(p);
            form.Photos.DataSource = DBManager.getPhotos(realeastate);
            form.Photos.AllowUserToAddRows = false;
            form.Photos.ReadOnly = true;
            form.Photos.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            this.Dispose();
        }
    }
}
