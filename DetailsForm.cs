﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace PKIprojekat
{
    public partial class DetailsForm : Form
    {
        public WindowForm WindowForm = null;

        public DetailsForm()
        {
            InitializeComponent();
        }

        protected virtual void CancelDetailsForm_Click(object sender, EventArgs e)
        {
            Dispose();
        }

        protected virtual void SaveDetailsForm_Click(object sender, EventArgs e)
        {
        }

        protected virtual void DeleteDetailsForm_Click(object sender, EventArgs e)
        {
        }
    }
}
