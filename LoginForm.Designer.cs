﻿using System.Globalization;
using System.Resources;
using System.Threading;

namespace PKIprojekat
{
    partial class LoginForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.LoginButton = new System.Windows.Forms.Button();
            this.UnameInput = new System.Windows.Forms.TextBox();
            this.PassInput = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.LoginErrorMessage = new System.Windows.Forms.Label();
            this.LoginEng = new System.Windows.Forms.Button();
            this.LoginSr = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // LoginButton
            // 
            this.LoginButton.Location = new System.Drawing.Point(175, 227);
            this.LoginButton.Name = "LoginButton";
            this.LoginButton.Size = new System.Drawing.Size(75, 23);
            this.LoginButton.TabIndex = 0;
            this.LoginButton.Text = "Prijavi se";
            this.LoginButton.UseVisualStyleBackColor = true;
            this.LoginButton.Click += new System.EventHandler(this.LoginButton_Click);
            // 
            // UnameInput
            // 
            this.UnameInput.Location = new System.Drawing.Point(107, 95);
            this.UnameInput.Name = "UnameInput";
            this.UnameInput.Size = new System.Drawing.Size(143, 20);
            this.UnameInput.TabIndex = 1;
            // 
            // PassInput
            // 
            this.PassInput.Location = new System.Drawing.Point(107, 121);
            this.PassInput.Name = "PassInput";
            this.PassInput.PasswordChar = '*';
            this.PassInput.Size = new System.Drawing.Size(143, 20);
            this.PassInput.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(23, 98);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(78, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Korisnicko ime:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(24, 124);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(80, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Lozinka:";
            this.label2.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // LoginErrorMessage
            // 
            this.LoginErrorMessage.AutoSize = true;
            this.LoginErrorMessage.ForeColor = System.Drawing.Color.DarkRed;
            this.LoginErrorMessage.Location = new System.Drawing.Point(104, 63);
            this.LoginErrorMessage.Name = "LoginErrorMessage";
            this.LoginErrorMessage.Size = new System.Drawing.Size(0, 13);
            this.LoginErrorMessage.TabIndex = 5;
            // 
            // LoginEng
            // 
            this.LoginEng.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.LoginEng.Location = new System.Drawing.Point(229, 12);
            this.LoginEng.Name = "LoginEng";
            this.LoginEng.Size = new System.Drawing.Size(43, 23);
            this.LoginEng.TabIndex = 6;
            this.LoginEng.Text = "eng";
            this.LoginEng.UseVisualStyleBackColor = true;
            this.LoginEng.Click += new System.EventHandler(this.LoginEng_Click);
            // 
            // LoginSr
            // 
            this.LoginSr.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.LoginSr.Location = new System.Drawing.Point(180, 12);
            this.LoginSr.Name = "LoginSr";
            this.LoginSr.Size = new System.Drawing.Size(43, 23);
            this.LoginSr.TabIndex = 7;
            this.LoginSr.Text = "srb";
            this.LoginSr.UseVisualStyleBackColor = true;
            this.LoginSr.Click += new System.EventHandler(this.LoginSr_Click);
            // 
            // LoginForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 262);
            this.Controls.Add(this.LoginSr);
            this.Controls.Add(this.LoginEng);
            this.Controls.Add(this.LoginErrorMessage);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.PassInput);
            this.Controls.Add(this.UnameInput);
            this.Controls.Add(this.LoginButton);
            this.Name = "LoginForm";
            this.Text = "Prijavljivanje";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button LoginButton;
        private System.Windows.Forms.TextBox UnameInput;
        private System.Windows.Forms.TextBox PassInput;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label LoginErrorMessage;

        public void setLanguage(int lang)
        {
            ResourceManager ResourceManager = null;
            if (lang == 0)
            {
                Thread.CurrentThread.CurrentUICulture = Thread.CurrentThread.CurrentCulture;
                ResourceManager = new ResourceManager("PKIprojekat.Resource.sr-SR", System.Reflection.Assembly.GetExecutingAssembly());
            }
            if (lang == 1)
            {
                Thread.CurrentThread.CurrentUICulture = Thread.CurrentThread.CurrentCulture;
                ResourceManager = new ResourceManager("PKIprojekat.Resource.en-EN", System.Reflection.Assembly.GetExecutingAssembly());
            }

            this.label1.Text = ResourceManager.GetString("username");
            this.label2.Text = ResourceManager.GetString("password");
            this.Text = ResourceManager.GetString("login");
            this.LoginButton.Text = ResourceManager.GetString("login");
        }

        private System.Windows.Forms.Button LoginEng;
        private System.Windows.Forms.Button LoginSr;
    }
}

