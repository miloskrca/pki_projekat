﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections;

using System.Globalization;
using System.Resources;
using System.Threading;


namespace PKIprojekat
{
    public partial class WindowForm : Form
    {
        public static int TAB_USER_DATA = 0;
        public static int TAB_USER_SEARCH = 1;
        public static int TAB_AGENCY_SEARCH = 2;
        public static int TAB_REALESTATE_SEARCH = 3;

        private DataGridView UserSearchResult = null;
        private DataGridView AgencySearchResult = null;
        private DataGridView RealestateSearchResult = null;
        public ResourceManager ResourceManager = null;
        public int role;
        public int user_id;
        public RealestateDetailsForm realestateForm;

        public WindowForm()
        {
            InitializeComponent();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        public TabControl getTabs()
        {
            return Tabs;
        }

        public void removeTab(int tabIndex)
        {
            getTabs().TabPages.RemoveAt(tabIndex);
        }
        
        static DataGridView searchUsers()
        {
            // Create a table
            DataGridView dTb = new DataGridView();
            Hashtable hash = new Hashtable();
            hash.Add(UserDetailsForm.COL_ID, CurrentForm.form.UserSearchID.Text);
            hash.Add(UserDetailsForm.COL_AGENCY, CurrentForm.form.UserSearchAgency.SelectedValue);
            hash.Add(UserDetailsForm.COL_FIRSTNAME, CurrentForm.form.UserSearchName.Text);
            hash.Add(UserDetailsForm.COL_LASTNAME, CurrentForm.form.UserSearchLastname.Text);
            hash.Add(UserDetailsForm.COL_ROLE, CurrentForm.form.UserSearchRole.SelectedValue);
            dTb.DataSource = DBManager.getUsersFromDatabase(hash);
            dTb.AllowUserToAddRows = false;
            dTb.ReadOnly = true;
            return dTb;
        }

        private void UserSearchView_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            DataGridViewCell id_cell = (DataGridViewCell)
                    UserSearchResult.Rows[e.RowIndex].Cells[0];

            String id = id_cell.Value.ToString();

            DataTable table = DBManager.executeQueryGet("SELECT * FROM " + UserDetailsForm.TABLE_NAME+ " WHERE "+UserDetailsForm.COL_ID+"=" + id);
            DataRow row = table.Rows[0];

            UserDetailsForm form = new UserDetailsForm();
            form.WindowForm = this;
            form.setLanguage();
            form.Id = row[UserDetailsForm.COL_ID].ToString();
            form.Username = row[UserDetailsForm.COL_USERNAME].ToString();
            form.Firstname = row[UserDetailsForm.COL_FIRSTNAME].ToString();
            form.Lastname = row[UserDetailsForm.COL_LASTNAME].ToString();
            
            form.reloadAgencies();
            form.Agency = int.Parse(row[UserDetailsForm.COL_AGENCY].ToString());
            form.Editing = true;
            form.EnableUsername(false);
            form.ShowDelete(true);
            form.Show();
            form.Role = int.Parse(row[UserDetailsForm.COL_ROLE].ToString());
        }

        private void UserCreate_Click(object sender, EventArgs e)
        {
            UserDetailsForm form = new UserDetailsForm();
            form.WindowForm = this;
            form.setLanguage();
            form.Id = "";
            form.Username = "";
            form.Firstname = "";
            form.Lastname = "";
            form.Role = 0;
            form.reloadAgencies();
            form.Editing = false;
            form.EnableUsername(true);
            form.ShowDelete(false);
            form.Show();
        }

        public void UserSearchSearch_Click(object sender, EventArgs e)
        {
            UserSearchResult = searchUsers();
            UserSearchResult.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;

            UserSearchResult.CellDoubleClick += new DataGridViewCellEventHandler(UserSearchView_CellContentClick);

            UserSearchResult.Dock = DockStyle.Fill;
            UserSearchResultsPanel.Controls.Clear();
            UserSearchResultsPanel.Controls.Add(UserSearchResult);

        }

        static DataGridView searchAgencies()
        {
            // Create a table
            DataGridView dTb = new DataGridView();
            Hashtable hash = new Hashtable();
            hash.Add(AgencyDetailsForm.COL_NAME, CurrentForm.form.AgencySearchName.Text);
            dTb.DataSource = DBManager.getAgenciesFromDatabase(hash);
            dTb.AllowUserToAddRows = false;
            dTb.ReadOnly = true;
            return dTb;
        }

        static DataGridView searchRealestates()
        {
            // Create a table
            DataGridView dTb = new DataGridView();
            Hashtable hash = new Hashtable();
            hash.Add(RealestateDetailsForm.COL_STRUCTURE, CurrentForm.form.RealSearchStructure.SelectedValue);
            hash.Add(RealestateDetailsForm.COL_AGENCY, CurrentForm.form.RealSearchAgency.SelectedValue);
            hash.Add(RealestateDetailsForm.COL_AGENT, CurrentForm.form.RealSearchAgent.SelectedValue);
            hash.Add(RealestateDetailsForm.COL_TYPE, CurrentForm.form.RealSearchType.SelectedValue);
            hash.Add(RealestateDetailsForm.COL_SALE_TYPE, CurrentForm.form.RealSearchSaleType.SelectedValue);
            hash.Add(RealestateDetailsForm.COL_STATUS, CurrentForm.form.RealSearchStatus.SelectedValue);
            hash.Add(RealestateDetailsForm.COL_PRICE + "_from", CurrentForm.form.RealSearchPriceFrom.Text);
            hash.Add(RealestateDetailsForm.COL_PRICE + "_to", CurrentForm.form.RealSearchPriceTo.Text);
            hash.Add(RealestateDetailsForm.COL_SURFACE + "_from", CurrentForm.form.RealSearchSurfaceFrom.Text);
            hash.Add(RealestateDetailsForm.COL_SURFACE + "_to", CurrentForm.form.RealSearchSurfaceTo.Text);
            hash.Add(RealestateDetailsForm.COL_FLOOR + "_from", CurrentForm.form.RealSearchFloorFrom.Text);
            hash.Add(RealestateDetailsForm.COL_FLOOR + "_to", CurrentForm.form.RealSearchFloorTo.Text);
            hash.Add(RealestateDetailsForm.COL_TERAS, CurrentForm.form.RealSearchTeras.Checked);
            hash.Add(RealestateDetailsForm.COL_BOOKED, CurrentForm.form.RealSearchBooked.Checked);
            dTb.DataSource = DBManager.getRealestatesFromDatabase(hash);
            dTb.AllowUserToAddRows = false;
            dTb.ReadOnly = true;
            return dTb;
        }

        private void AgencySearchView_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            DataGridViewCell id_cell = (DataGridViewCell)
                    AgencySearchResult.Rows[e.RowIndex].Cells[0];

            String id = id_cell.Value.ToString();

            DataTable table = DBManager.executeQueryGet("SELECT * FROM " + AgencyDetailsForm.TABLE_NAME + " WHERE " + AgencyDetailsForm.COL_ID + "=" + id);
            DataRow row = table.Rows[0];

            AgencyDetailsForm form = new AgencyDetailsForm();
            form.WindowForm = this;
            form.setLanguage();
            form.AgencyId = row[AgencyDetailsForm.COL_ID].ToString();
            form.AgencyName = row[AgencyDetailsForm.COL_NAME].ToString();
            form.AgencyAddress = row[AgencyDetailsForm.COL_ADDRESS].ToString();
            form.AgencyPhone = row[AgencyDetailsForm.COL_PHONE].ToString();
            form.AgencyEmail = row[AgencyDetailsForm.COL_EMAIL].ToString();
            form.Editing = true;
            form.ShowDelete(true);
            form.Show();
        }

        private void RealestateSearchView_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            DataGridViewCell id_cell = (DataGridViewCell)
                    RealestateSearchResult.Rows[e.RowIndex].Cells[0];

            String id = id_cell.Value.ToString();

            DataTable table = DBManager.executeQueryGet("SELECT * FROM " + RealestateDetailsForm.TABLE_NAME + " WHERE " + RealestateDetailsForm.COL_ID + "=" + id);
            DataRow row = table.Rows[0];

            RealestateDetailsForm form = new RealestateDetailsForm();
            form.WindowForm = this;
            realestateForm = form;

            form.reloadAgencies();
            form.reloadAgents();
            form.setLanguage();

            form.RealId = row[RealestateDetailsForm.COL_ID].ToString();
            form.RealDate = row[RealestateDetailsForm.COL_DATE].ToString();
            form.RealPrice = row[RealestateDetailsForm.COL_PRICE].ToString();
            form.RealFloor = row[RealestateDetailsForm.COL_FLOOR].ToString();
            form.RealSurface = row[RealestateDetailsForm.COL_SURFACE].ToString();
            
            form.RealDescription = row[RealestateDetailsForm.COL_DESCRIPTION].ToString();
            form.RealFinancing = row[RealestateDetailsForm.COL_FINANCING].ToString();
            form.Editing = true;
            form.ShowDelete(true);
            form.Show();
            form.RealAgent = int.Parse(row[RealestateDetailsForm.COL_AGENT].ToString());
            form.RealAgency = int.Parse(row[RealestateDetailsForm.COL_AGENCY].ToString());
            form.RealSaleType = int.Parse(row[RealestateDetailsForm.COL_SALE_TYPE].ToString());
            form.RealHeating = int.Parse(row[RealestateDetailsForm.COL_HEATING].ToString());
            form.RealType = int.Parse(row[RealestateDetailsForm.COL_TYPE].ToString());
            form.RealStatus = int.Parse(row[RealestateDetailsForm.COL_STATUS].ToString());
            form.RealStructure = int.Parse(row[RealestateDetailsForm.COL_STRUCTURE].ToString());
            form.RealTeras = int.Parse(row[RealestateDetailsForm.COL_TERAS].ToString());
            form.RealBooked = int.Parse(row[RealestateDetailsForm.COL_BOOKED].ToString());
            form.RealRooms = row["rooms"].ToString();
            form.RealFinished = int.Parse(row["finished"].ToString());

            form.same = user_id == form.RealAgent;
            if (!form.same)
            {
                form.button3.Visible = false;
            }

            form.Activities.DataSource = DBManager.getActivities(int.Parse(row[RealestateDetailsForm.COL_ID].ToString()), user_id == form.RealAgent);
            form.Activities.AllowUserToAddRows = false;
            form.Activities.ReadOnly = true;
            form.Activities.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            form.Activities.CellDoubleClick += new DataGridViewCellEventHandler(ActivityDoubleClick);

            form.Documents.DataSource = DBManager.getDocuments(int.Parse(row[RealestateDetailsForm.COL_ID].ToString()));
            form.Documents.AllowUserToAddRows = false;
            form.Documents.ReadOnly = true;
            form.Documents.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;

            form.Photos.DataSource = DBManager.getPhotos(int.Parse(row[RealestateDetailsForm.COL_ID].ToString()));
            form.Photos.AllowUserToAddRows = false;
            form.Photos.ReadOnly = true;
            form.Photos.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;

            if (role == 3)
            {
                form.RealDataAgent.Enabled = false;
            }

        }

        private void AgencySearchSearch_Click(object sender, EventArgs e)
        {
            AgencySearchResult = searchAgencies();
            AgencySearchResult.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;

            AgencySearchResult.CellDoubleClick += new DataGridViewCellEventHandler(AgencySearchView_CellContentClick);

            AgencySearchResult.Dock = DockStyle.Fill;
            AgencySearchResultsPanel.Controls.Clear();
            AgencySearchResultsPanel.Controls.Add(AgencySearchResult);

        }

        private void RealestateSearchSearch_Click(object sender, EventArgs e)
        {
            RealestateSearchResult = searchRealestates();
            RealestateSearchResult.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;

            RealestateSearchResult.CellDoubleClick += new DataGridViewCellEventHandler(RealestateSearchView_CellContentClick);

            RealestateSearchResult.Dock = DockStyle.Fill;
            RealestateSearchResultsPanel.Controls.Clear();
            RealestateSearchResultsPanel.Controls.Add(RealestateSearchResult);
        }

        private void ActivityDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            DataGridViewCell id_cell = (DataGridViewCell)
                    realestateForm.Activities.Rows[e.RowIndex].Cells[0];
            DataGridViewCell user = (DataGridViewCell)
                    realestateForm.Activities.Rows[e.RowIndex].Cells[2];


            String id = id_cell.Value.ToString();

            if (user_id != int.Parse(user.Value.ToString())) return;

            DataTable table = DBManager.executeQueryGet("SELECT * FROM tbl_activity WHERE id=" + id);
            DataRow row = table.Rows[0];

            ActivityForm form = new ActivityForm();
            form.realeastate = int.Parse(realestateForm.RealId);
            form.form = realestateForm;
            form.same = realestateForm.same;
            DBManager.ImportInCombo(form.comboBox1, UserDetailsForm.TABLE_NAME, UserDetailsForm.COL_USERNAME, UserDetailsForm.COL_ID);

            form.comboBox1.SelectedValue = user_id;
            form.comboBox1.Enabled = false;
            form.dateTimePicker1.Text = row["date"].ToString();
            form.richTextBox1.Text = row["remark"].ToString();
            form.textBox1.Text = row["client"].ToString();
            form.button3.Visible = true;
            form.id = row["id"].ToString();
            form.edit = true;
            form.ShowDialog();

        }

        private void AgencyCreate_Click(object sender, EventArgs e)
        {
            AgencyDetailsForm form = new AgencyDetailsForm();
            form.WindowForm = this;
            form.setLanguage();
            form.AgencyId = "";
            form.AgencyName = "";
            form.AgencyPhone = "";
            form.AgencyEmail = "";
            form.AgencyAddress = "";
            form.Editing = false;
            form.ShowDelete(false);
            form.Show();
        }

        private void RealestateCreate_Click(object sender, EventArgs e)
        {
            RealestateDetailsForm form = new RealestateDetailsForm();
            form.WindowForm = this;
            
            form.reloadAgencies();
            form.reloadAgents();
            form.setLanguage();

            form.RealPrice = "";
            form.RealSurface = "";
            form.RealStructure = 0;
            form.RealFloor = "";
            form.RealHeating = 0;
            form.RealTeras = 0;
            form.RealType = 0;
            form.RealDate = "";
            form.RealBooked = 0;
            form.RealStatus = 0;
            form.RealSaleType = 0;
            form.RealAgency = 0;
            form.RealAgent = 0;
            form.RealDescription = "";
            form.RealFinancing = "";
            form.RealFinished = 0;
            form.RealRooms = "";
            form.Editing = false;
            form.ShowDelete(false);
            form.Show();
        }

        private void LogOutMenuItem_Click(object sender, EventArgs e)
        {
            LoginForm login = new LoginForm();
            login.Show();
            CurrentForm.form = null;
            this.Dispose();
        }

        private void UserDataSave_Click(object sender, EventArgs e)
        {
            string id = DataId;
            string password = DataPassword;
            string name = DataName;
            string lastame = DataLastname;

            Hashtable hash = new Hashtable();
            hash.Add(UserDetailsForm.COL_FIRSTNAME, name);
            hash.Add(UserDetailsForm.COL_PASSWORD, password);
            hash.Add(UserDetailsForm.COL_LASTNAME, lastame);

            DBManager.executeQueryUpdate(UserDetailsForm.TABLE_NAME, hash, UserDetailsForm.COL_ID, id);
        }

        private void WindowForm_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'users.tbl_users' table. You can move, or remove it, as needed.
            this.tbl_usersTableAdapter.Fill(this.users.tbl_users);
            // TODO: This line of code loads data into the 'roles.tbl_role' table. You can move, or remove it, as needed.
            this.tbl_roleTableAdapter.Fill(this.roles.tbl_role);
            // TODO: This line of code loads data into the 'agencies.tbl_agency' table. You can move, or remove it, as needed.
            this.tbl_agencyTableAdapter.Fill(this.agencies.tbl_agency);
            // TODO: This line of code loads data into the 'saleType.tbl_sale_type' table. You can move, or remove it, as needed.
            this.tbl_sale_typeTableAdapter.Fill(this.saleType.tbl_sale_type);
            // TODO: This line of code loads data into the 'status.tbl_realestate_status' table. You can move, or remove it, as needed.
            this.tbl_realestate_statusTableAdapter.Fill(this.status.tbl_realestate_status);
            // TODO: This line of code loads data into the 'realetateType.tbl_realestate_type' table. You can move, or remove it, as needed.
            this.tbl_realestate_typeTableAdapter.Fill(this.realetateType.tbl_realestate_type);
            // TODO: This line of code loads data into the 'heating.tbl_heating' table. You can move, or remove it, as needed.
            this.tbl_heatingTableAdapter.Fill(this.heating.tbl_heating);
            // TODO: This line of code loads data into the 'structure.tbl_structure' table. You can move, or remove it, as needed.
            this.tbl_structureTableAdapter.Fill(this.structure.tbl_structure);
        }

        private void englishToolStripMenuItem_Click(object sender, EventArgs e)
        {
            setLanguage(1);
        }

        private void serbianToolStripMenuItem_Click(object sender, EventArgs e)
        {
            setLanguage(0);
        }
    }
}