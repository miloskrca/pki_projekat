﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections;

namespace PKIprojekat
{
    public partial class UserDetailsForm : DetailsForm
    {

        public static string TABLE_NAME = "tbl_users";
        public static string COL_USERNAME = "username";
        public static string COL_FIRSTNAME = "firstname";
        public static string COL_LASTNAME = "lastname";
        public static string COL_PASSWORD = "password";
        public static string COL_ID = "id_user";
        public static string COL_ROLE = "role";
        public static string COL_AGENCY = "agency";

        public UserDetailsForm()
        {
            InitializeComponent();
        }

        protected override void SaveDetailsForm_Click(object sender, EventArgs e)
        {
            if (Editing)
            {
                Hashtable hash = new Hashtable();
                hash.Add(COL_USERNAME, Username);
                hash.Add(COL_FIRSTNAME, Firstname);
                hash.Add(COL_LASTNAME, Lastname);
                hash.Add(COL_ROLE, Role);
                hash.Add(COL_AGENCY, Agency);
                DBManager.executeQueryUpdate(TABLE_NAME, hash, COL_ID, Id);
            }
            else
            {
                Hashtable hash = new Hashtable();
                hash.Add(COL_USERNAME, Username);
                hash.Add(COL_PASSWORD, "");
                hash.Add(COL_FIRSTNAME, Firstname);
                hash.Add(COL_LASTNAME, Lastname);
                hash.Add(COL_ROLE, Role);
                hash.Add(COL_AGENCY, Agency);
                DBManager.executeQueryInsert(TABLE_NAME, hash);
                DBManager.ImportInCombo(CurrentForm.form.RealSearchAgent, UserDetailsForm.TABLE_NAME, UserDetailsForm.COL_USERNAME, UserDetailsForm.COL_ID);
            }
            Dispose();
        }

        protected override void DeleteDetailsForm_Click(object sender, EventArgs e)
        {
            DBManager.executeQueryDelete(TABLE_NAME, COL_ID, Id);
            DBManager.ImportInCombo(CurrentForm.form.RealSearchAgent, UserDetailsForm.TABLE_NAME, UserDetailsForm.COL_USERNAME, UserDetailsForm.COL_ID);
            Dispose();
        }

        private void UserDetailsForm_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'roles.tbl_role' table. You can move, or remove it, as needed.
            this.tbl_roleTableAdapter.Fill(this.roles.tbl_role);

        }
    }
}
