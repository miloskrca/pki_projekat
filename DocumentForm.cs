﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace PKIprojekat
{
    public partial class DocumentForm : Form
    {
        public int realeastate;
        public RealestateDetailsForm form;

        public DocumentForm()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Document d = new Document(
                realeastate,
                this.dateTimePicker1.Text,
                this.textBox1.Text,
                this.richTextBox1.Text);
            DBManager.insertDocument(d);
            form.Documents.DataSource = DBManager.getDocuments(realeastate);
            form.Documents.AllowUserToAddRows = false;
            form.Documents.ReadOnly = true;
            form.Documents.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            this.Dispose();
        }
    }
}
