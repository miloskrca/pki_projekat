﻿namespace PKIprojekat
{
    partial class AgencyDetailsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.AgencyDataPhone = new System.Windows.Forms.TextBox();
            this.AgencyDataAddress = new System.Windows.Forms.TextBox();
            this.AgencyDataName = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.AgencyDataEmail = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // AgencyDataPhone
            // 
            this.AgencyDataPhone.Location = new System.Drawing.Point(248, 169);
            this.AgencyDataPhone.Name = "AgencyDataPhone";
            this.AgencyDataPhone.Size = new System.Drawing.Size(203, 20);
            this.AgencyDataPhone.TabIndex = 29;
            // 
            // AgencyDataAddress
            // 
            this.AgencyDataAddress.Location = new System.Drawing.Point(248, 143);
            this.AgencyDataAddress.Name = "AgencyDataAddress";
            this.AgencyDataAddress.Size = new System.Drawing.Size(203, 20);
            this.AgencyDataAddress.TabIndex = 28;
            // 
            // AgencyDataName
            // 
            this.AgencyDataName.Location = new System.Drawing.Point(248, 117);
            this.AgencyDataName.Name = "AgencyDataName";
            this.AgencyDataName.Size = new System.Drawing.Size(203, 20);
            this.AgencyDataName.TabIndex = 27;
            // 
            // label4
            // 
            this.label4.Location = new System.Drawing.Point(185, 172);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(60, 13);
            this.label4.TabIndex = 25;
            this.label4.Text = "Telefon:";
            this.label4.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(185, 146);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(60, 13);
            this.label3.TabIndex = 24;
            this.label3.Text = "Adresa:";
            this.label3.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(185, 120);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(60, 13);
            this.label1.TabIndex = 22;
            this.label1.Text = "Ime:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // AgencyDataEmail
            // 
            this.AgencyDataEmail.Location = new System.Drawing.Point(248, 195);
            this.AgencyDataEmail.Name = "AgencyDataEmail";
            this.AgencyDataEmail.Size = new System.Drawing.Size(203, 20);
            this.AgencyDataEmail.TabIndex = 31;
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(185, 198);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(60, 13);
            this.label2.TabIndex = 30;
            this.label2.Text = "E-Posta:";
            this.label2.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // AgencyDetailsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(617, 361);
            this.Controls.Add(this.AgencyDataEmail);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.AgencyDataPhone);
            this.Controls.Add(this.AgencyDataAddress);
            this.Controls.Add(this.AgencyDataName);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label1);
            this.Name = "AgencyDetailsForm";
            this.Text = "AgencyDetailsForm";
            this.Controls.SetChildIndex(this.SaveDetailsForm, 0);
            this.Controls.SetChildIndex(this.CancelDetailsForm, 0);
            this.Controls.SetChildIndex(this.DeleteDetailsForm, 0);
            this.Controls.SetChildIndex(this.label1, 0);
            this.Controls.SetChildIndex(this.label3, 0);
            this.Controls.SetChildIndex(this.label4, 0);
            this.Controls.SetChildIndex(this.AgencyDataName, 0);
            this.Controls.SetChildIndex(this.AgencyDataAddress, 0);
            this.Controls.SetChildIndex(this.AgencyDataPhone, 0);
            this.Controls.SetChildIndex(this.label2, 0);
            this.Controls.SetChildIndex(this.AgencyDataEmail, 0);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public static string TABLE_NAME = "tbl_agency";
        public static string COL_ADDRESS = "address";
        public static string COL_NAME = "name";
        public static string COL_PHONE = "phone";
        public static string COL_EMAIL = "email";
        public static string COL_ID = "id_agency";
        
        private System.Windows.Forms.TextBox AgencyDataPhone;
        private System.Windows.Forms.TextBox AgencyDataAddress;
        private System.Windows.Forms.TextBox AgencyDataName;
        private System.Windows.Forms.TextBox AgencyDataEmail;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;

        public string AgencyAddress
        {
            get { return AgencyDataAddress.Text; }
            set { AgencyDataAddress.Text = value; }
        }

        public string AgencyName
        {
            get { return AgencyDataName.Text; }
            set { AgencyDataName.Text = value; }
        }

        public string AgencyPhone
        {
            get { return AgencyDataPhone.Text; }
            set { AgencyDataPhone.Text = value; }
        }

        public string AgencyEmail
        {
            get { return AgencyDataEmail.Text; }
            set { AgencyDataEmail.Text = value; }
        }

        private string _id = null;
        public string AgencyId
        {
            get { return _id; }
            set { _id = value; }
        }

        public void setLanguage()
        {
            this.label1.Text = CurrentForm.form.ResourceManager.GetString("name");
            this.label2.Text = CurrentForm.form.ResourceManager.GetString("address");
            this.label3.Text = CurrentForm.form.ResourceManager.GetString("phone");
            this.label4.Text = CurrentForm.form.ResourceManager.GetString("email");
            this.DeleteDetailsForm.Text = CurrentForm.form.ResourceManager.GetString("delete");
            this.SaveDetailsForm.Text = CurrentForm.form.ResourceManager.GetString("save");
            this.CancelDetailsForm.Text = CurrentForm.form.ResourceManager.GetString("cancel");
            this.Text = CurrentForm.form.ResourceManager.GetString("agency_details");
        }

    }
}