﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PKIprojekat
{
    class Document
    {
        public string date;
        public int realestate;
        public string type;
        public string remark;

        public Document(int re, string d, string t, string r)
        {
            realestate = re;
            date = d;
            type = t;
            remark = r;
        }
    }
}
