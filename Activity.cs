﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PKIprojekat
{
    class Activity
    {
        public string date;
        public int agent;
        public int realestate;
        public string client;
        public string remark;

        public Activity(int re, string d, int a, string c, string r)
        {
            realestate = re;
            date = d;
            agent = a;
            client = c;
            remark = r;
        }
    }
}
