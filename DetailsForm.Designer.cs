﻿namespace PKIprojekat
{
    partial class DetailsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.SaveDetailsForm = new System.Windows.Forms.Button();
            this.CancelDetailsForm = new System.Windows.Forms.Button();
            this.DeleteDetailsForm = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // SaveDetailsForm
            // 
            this.SaveDetailsForm.Location = new System.Drawing.Point(379, 300);
            this.SaveDetailsForm.Name = "SaveDetailsForm";
            this.SaveDetailsForm.Size = new System.Drawing.Size(110, 23);
            this.SaveDetailsForm.TabIndex = 0;
            this.SaveDetailsForm.Text = "Sacuvaj";
            this.SaveDetailsForm.UseVisualStyleBackColor = true;
            this.SaveDetailsForm.Click += new System.EventHandler(this.SaveDetailsForm_Click);
            // 
            // CancelDetailsForm
            // 
            this.CancelDetailsForm.Location = new System.Drawing.Point(495, 300);
            this.CancelDetailsForm.Name = "CancelDetailsForm";
            this.CancelDetailsForm.Size = new System.Drawing.Size(110, 23);
            this.CancelDetailsForm.TabIndex = 1;
            this.CancelDetailsForm.Text = "Izadji";
            this.CancelDetailsForm.UseVisualStyleBackColor = true;
            this.CancelDetailsForm.Click += new System.EventHandler(this.CancelDetailsForm_Click);
            // 
            // DeleteDetailsForm
            // 
            this.DeleteDetailsForm.Location = new System.Drawing.Point(12, 300);
            this.DeleteDetailsForm.Name = "DeleteDetailsForm";
            this.DeleteDetailsForm.Size = new System.Drawing.Size(110, 23);
            this.DeleteDetailsForm.TabIndex = 2;
            this.DeleteDetailsForm.Text = "Obrisi";
            this.DeleteDetailsForm.UseVisualStyleBackColor = true;
            this.DeleteDetailsForm.Click += new System.EventHandler(this.DeleteDetailsForm_Click);
            // 
            // DetailsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(617, 361);
            this.Controls.Add(this.DeleteDetailsForm);
            this.Controls.Add(this.CancelDetailsForm);
            this.Controls.Add(this.SaveDetailsForm);
            this.Name = "DetailsForm";
            this.Text = "DetailsForm";
            this.ResumeLayout(false);

        }

        #endregion

        public System.Windows.Forms.Button SaveDetailsForm;
        public System.Windows.Forms.Button CancelDetailsForm;

        private bool _editing = false;
        public System.Windows.Forms.Button DeleteDetailsForm;

        public bool Editing
        {
            get { return _editing; }
            set { _editing = value; }
        }

        public void ShowDelete(bool b)
        {
            DeleteDetailsForm.Visible = b;
        }
    }
}