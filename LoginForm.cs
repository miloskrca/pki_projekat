﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace PKIprojekat
{
    public partial class LoginForm : Form
    {

        private int language = 1;

        public LoginForm()
        {
            InitializeComponent();
            setLanguage(language);
        }

        public void showError(string text)
        {
            LoginErrorMessage.Text = text;
        }

        private void LoginButton_Click(object sender, EventArgs e)
        {
            if (DBManager.CheckLogin(UnameInput.Text, PassInput.Text))
            {
                CurrentForm.form = new WindowForm();
                CurrentForm.form.setLanguage(language);
                DBManager.ManageTabs(UnameInput.Text, CurrentForm.form);
                Hide();
                CurrentForm.form.Show();
            }
            else
            {
                showError("Pogresno uneti podaci!");
            }
        }

        private void LoginEng_Click(object sender, EventArgs e)
        {
            language = 1;
            setLanguage(language);
        }

        private void LoginSr_Click(object sender, EventArgs e)
        {
            language = 0;
            setLanguage(language);
        }
    }
}
