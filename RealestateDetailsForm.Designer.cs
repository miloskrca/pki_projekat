﻿using System.Collections.Generic;
using System.Globalization;
using System.Resources;
using System.Threading;

namespace PKIprojekat
{
    partial class RealestateDetailsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.RealDataBooked = new System.Windows.Forms.CheckBox();
            this.RealDataTeras = new System.Windows.Forms.CheckBox();
            this.RealDataAgency = new System.Windows.Forms.ComboBox();
            this.label19 = new System.Windows.Forms.Label();
            this.RealDataStatus = new System.Windows.Forms.ComboBox();
            this.label26 = new System.Windows.Forms.Label();
            this.RealDataDate = new System.Windows.Forms.DateTimePicker();
            this.RealDataSaleType = new System.Windows.Forms.ComboBox();
            this.label25 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.RealDataType = new System.Windows.Forms.ComboBox();
            this.label16 = new System.Windows.Forms.Label();
            this.RealDataHeating = new System.Windows.Forms.ComboBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.RealDataFloor = new System.Windows.Forms.TextBox();
            this.RealDataStructure = new System.Windows.Forms.ComboBox();
            this.label22 = new System.Windows.Forms.Label();
            this.RealDataSurface = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.RealDataPrice = new System.Windows.Forms.TextBox();
            this.RealDataAgent = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.RealDataFinancing = new System.Windows.Forms.RichTextBox();
            this.RealDataDescription = new System.Windows.Forms.RichTextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.Activities = new System.Windows.Forms.DataGridView();
            this.Documents = new System.Windows.Forms.DataGridView();
            this.label3 = new System.Windows.Forms.Label();
            this.Photos = new System.Windows.Forms.DataGridView();
            this.label7 = new System.Windows.Forms.Label();
            this.button3 = new System.Windows.Forms.Button();
            this.RealDataFinished = new System.Windows.Forms.CheckBox();
            this.RealDataRooms = new System.Windows.Forms.RichTextBox();
            ((System.ComponentModel.ISupportInitialize)(this.Activities)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Documents)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Photos)).BeginInit();
            this.SuspendLayout();
            // 
            // SaveDetailsForm
            // 
            this.SaveDetailsForm.Location = new System.Drawing.Point(884, 573);
            // 
            // CancelDetailsForm
            // 
            this.CancelDetailsForm.Location = new System.Drawing.Point(1000, 573);
            // 
            // DeleteDetailsForm
            // 
            this.DeleteDetailsForm.Location = new System.Drawing.Point(517, 573);
            // 
            // RealDataBooked
            // 
            this.RealDataBooked.AutoSize = true;
            this.RealDataBooked.Location = new System.Drawing.Point(355, 18);
            this.RealDataBooked.Name = "RealDataBooked";
            this.RealDataBooked.Size = new System.Drawing.Size(73, 17);
            this.RealDataBooked.TabIndex = 90;
            this.RealDataBooked.Text = "Uknjizeno";
            this.RealDataBooked.UseVisualStyleBackColor = true;
            // 
            // RealDataTeras
            // 
            this.RealDataTeras.AutoSize = true;
            this.RealDataTeras.Location = new System.Drawing.Point(284, 18);
            this.RealDataTeras.Name = "RealDataTeras";
            this.RealDataTeras.Size = new System.Drawing.Size(59, 17);
            this.RealDataTeras.TabIndex = 89;
            this.RealDataTeras.Text = "Terasa";
            this.RealDataTeras.UseVisualStyleBackColor = true;
            // 
            // RealDataAgency
            // 
            this.RealDataAgency.FormattingEnabled = true;
            this.RealDataAgency.Location = new System.Drawing.Point(76, 259);
            this.RealDataAgency.Name = "RealDataAgency";
            this.RealDataAgency.Size = new System.Drawing.Size(137, 21);
            this.RealDataAgency.TabIndex = 88;
            // 
            // label19
            // 
            this.label19.Location = new System.Drawing.Point(5, 262);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(65, 13);
            this.label19.TabIndex = 87;
            this.label19.Text = "Agencija:";
            this.label19.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // RealDataStatus
            // 
            this.RealDataStatus.DisplayMember = "id_realestate_status";
            this.RealDataStatus.FormattingEnabled = true;
            this.RealDataStatus.Location = new System.Drawing.Point(76, 232);
            this.RealDataStatus.Name = "RealDataStatus";
            this.RealDataStatus.Size = new System.Drawing.Size(137, 21);
            this.RealDataStatus.TabIndex = 84;
            this.RealDataStatus.ValueMember = "id_realestate_status";
            // 
            // label26
            // 
            this.label26.Location = new System.Drawing.Point(5, 235);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(65, 13);
            this.label26.TabIndex = 83;
            this.label26.Text = "Status:";
            this.label26.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // RealDataDate
            // 
            this.RealDataDate.Location = new System.Drawing.Point(76, 13);
            this.RealDataDate.Name = "RealDataDate";
            this.RealDataDate.Size = new System.Drawing.Size(196, 20);
            this.RealDataDate.TabIndex = 80;
            // 
            // RealDataSaleType
            // 
            this.RealDataSaleType.DisplayMember = "id_sale_type";
            this.RealDataSaleType.FormattingEnabled = true;
            this.RealDataSaleType.Location = new System.Drawing.Point(76, 152);
            this.RealDataSaleType.Name = "RealDataSaleType";
            this.RealDataSaleType.Size = new System.Drawing.Size(137, 21);
            this.RealDataSaleType.TabIndex = 86;
            this.RealDataSaleType.ValueMember = "id_sale_type";
            // 
            // label25
            // 
            this.label25.Location = new System.Drawing.Point(10, 19);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(65, 13);
            this.label25.TabIndex = 79;
            this.label25.Text = "Datum:";
            this.label25.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label18
            // 
            this.label18.Location = new System.Drawing.Point(5, 155);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(65, 13);
            this.label18.TabIndex = 85;
            this.label18.Text = "Tip prodaje:";
            this.label18.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // RealDataType
            // 
            this.RealDataType.DisplayMember = "id_realestate_type";
            this.RealDataType.FormattingEnabled = true;
            this.RealDataType.Location = new System.Drawing.Point(76, 205);
            this.RealDataType.Name = "RealDataType";
            this.RealDataType.Size = new System.Drawing.Size(137, 21);
            this.RealDataType.TabIndex = 78;
            this.RealDataType.ValueMember = "id_realestate_type";
            // 
            // label16
            // 
            this.label16.Location = new System.Drawing.Point(5, 208);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(65, 13);
            this.label16.TabIndex = 77;
            this.label16.Text = "Tip:";
            this.label16.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // RealDataHeating
            // 
            this.RealDataHeating.DisplayMember = "id_heating";
            this.RealDataHeating.FormattingEnabled = true;
            this.RealDataHeating.Location = new System.Drawing.Point(76, 179);
            this.RealDataHeating.Name = "RealDataHeating";
            this.RealDataHeating.Size = new System.Drawing.Size(137, 21);
            this.RealDataHeating.TabIndex = 76;
            this.RealDataHeating.ValueMember = "id_heating";
            // 
            // label15
            // 
            this.label15.Location = new System.Drawing.Point(5, 182);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(65, 13);
            this.label15.TabIndex = 75;
            this.label15.Text = "Grejanje:";
            this.label15.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label24
            // 
            this.label24.Location = new System.Drawing.Point(6, 128);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(65, 13);
            this.label24.TabIndex = 71;
            this.label24.Text = "Sprat:";
            this.label24.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // RealDataFloor
            // 
            this.RealDataFloor.Location = new System.Drawing.Point(76, 125);
            this.RealDataFloor.Name = "RealDataFloor";
            this.RealDataFloor.Size = new System.Drawing.Size(137, 20);
            this.RealDataFloor.TabIndex = 72;
            // 
            // RealDataStructure
            // 
            this.RealDataStructure.DisplayMember = "id_structure";
            this.RealDataStructure.FormattingEnabled = true;
            this.RealDataStructure.Location = new System.Drawing.Point(76, 98);
            this.RealDataStructure.Name = "RealDataStructure";
            this.RealDataStructure.Size = new System.Drawing.Size(137, 21);
            this.RealDataStructure.TabIndex = 70;
            this.RealDataStructure.ValueMember = "id_structure";
            // 
            // label22
            // 
            this.label22.Location = new System.Drawing.Point(7, 75);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(65, 13);
            this.label22.TabIndex = 66;
            this.label22.Text = "Povrsina:";
            this.label22.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // RealDataSurface
            // 
            this.RealDataSurface.Location = new System.Drawing.Point(76, 72);
            this.RealDataSurface.Name = "RealDataSurface";
            this.RealDataSurface.Size = new System.Drawing.Size(137, 20);
            this.RealDataSurface.TabIndex = 67;
            // 
            // label14
            // 
            this.label14.Location = new System.Drawing.Point(7, 102);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(65, 13);
            this.label14.TabIndex = 63;
            this.label14.Text = "Struktura:";
            this.label14.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label11
            // 
            this.label11.Location = new System.Drawing.Point(5, 48);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(65, 13);
            this.label11.TabIndex = 60;
            this.label11.Text = "Cena:";
            this.label11.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // RealDataPrice
            // 
            this.RealDataPrice.Location = new System.Drawing.Point(76, 45);
            this.RealDataPrice.Name = "RealDataPrice";
            this.RealDataPrice.Size = new System.Drawing.Size(137, 20);
            this.RealDataPrice.TabIndex = 61;
            // 
            // RealDataAgent
            // 
            this.RealDataAgent.FormattingEnabled = true;
            this.RealDataAgent.Location = new System.Drawing.Point(284, 48);
            this.RealDataAgent.Name = "RealDataAgent";
            this.RealDataAgent.Size = new System.Drawing.Size(137, 21);
            this.RealDataAgent.TabIndex = 92;
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(219, 48);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(65, 13);
            this.label1.TabIndex = 91;
            this.label1.Text = "Agent:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(452, 75);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(137, 23);
            this.button1.TabIndex = 95;
            this.button1.Text = "Dodaj fotografiju";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(452, 102);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(137, 23);
            this.button2.TabIndex = 96;
            this.button2.Text = "Dodaj dokument";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // RealDataFinancing
            // 
            this.RealDataFinancing.Location = new System.Drawing.Point(236, 98);
            this.RealDataFinancing.Name = "RealDataFinancing";
            this.RealDataFinancing.Size = new System.Drawing.Size(185, 177);
            this.RealDataFinancing.TabIndex = 100;
            this.RealDataFinancing.Text = global::PKIprojekat.Resource_sr_SR.String1;
            // 
            // RealDataDescription
            // 
            this.RealDataDescription.Location = new System.Drawing.Point(76, 286);
            this.RealDataDescription.Name = "RealDataDescription";
            this.RealDataDescription.Size = new System.Drawing.Size(345, 174);
            this.RealDataDescription.TabIndex = 101;
            this.RealDataDescription.Text = global::PKIprojekat.Resource_sr_SR.String1;
            // 
            // label4
            // 
            this.label4.Location = new System.Drawing.Point(7, 289);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(65, 13);
            this.label4.TabIndex = 102;
            this.label4.Text = "Opis:";
            this.label4.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(592, 22);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(56, 13);
            this.label5.TabIndex = 103;
            this.label5.Text = "Aktivnosti:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(233, 75);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(51, 13);
            this.label6.TabIndex = 104;
            this.label6.Text = "Finansije:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(17, 466);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 13);
            this.label2.TabIndex = 106;
            this.label2.Text = "Prostorije:";
            // 
            // Activities
            // 
            this.Activities.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.Activities.Location = new System.Drawing.Point(595, 45);
            this.Activities.Name = "Activities";
            this.Activities.Size = new System.Drawing.Size(565, 230);
            this.Activities.TabIndex = 108;
            // 
            // Documents
            // 
            this.Documents.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.Documents.Location = new System.Drawing.Point(452, 313);
            this.Documents.Name = "Documents";
            this.Documents.Size = new System.Drawing.Size(350, 245);
            this.Documents.TabIndex = 110;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(449, 297);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(61, 13);
            this.label3.TabIndex = 109;
            this.label3.Text = "Dokumenti:";
            // 
            // Photos
            // 
            this.Photos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.Photos.Location = new System.Drawing.Point(823, 313);
            this.Photos.Name = "Photos";
            this.Photos.Size = new System.Drawing.Size(337, 245);
            this.Photos.TabIndex = 112;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(820, 297);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(59, 13);
            this.label7.TabIndex = 111;
            this.label7.Text = "Fotografije:";
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(452, 46);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(137, 23);
            this.button3.TabIndex = 113;
            this.button3.Text = "Dodaj Aktivnost";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // RealDataFinished
            // 
            this.RealDataFinished.AutoSize = true;
            this.RealDataFinished.Location = new System.Drawing.Point(463, 258);
            this.RealDataFinished.Name = "RealDataFinished";
            this.RealDataFinished.Size = new System.Drawing.Size(79, 17);
            this.RealDataFinished.TabIndex = 114;
            this.RealDataFinished.Text = "Zakljuceno";
            this.RealDataFinished.UseVisualStyleBackColor = true;
            // 
            // RealDataRooms
            // 
            this.RealDataRooms.Location = new System.Drawing.Point(76, 466);
            this.RealDataRooms.Name = "RealDataRooms";
            this.RealDataRooms.Size = new System.Drawing.Size(352, 92);
            this.RealDataRooms.TabIndex = 115;
            this.RealDataRooms.Text = global::PKIprojekat.Resource_sr_SR.String1;
            // 
            // RealestateDetailsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1179, 620);
            this.Controls.Add(this.RealDataRooms);
            this.Controls.Add(this.RealDataFinished);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.Photos);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.Documents);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.Activities);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.RealDataDescription);
            this.Controls.Add(this.RealDataFinancing);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.RealDataAgent);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.RealDataBooked);
            this.Controls.Add(this.RealDataTeras);
            this.Controls.Add(this.RealDataAgency);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.RealDataStatus);
            this.Controls.Add(this.label26);
            this.Controls.Add(this.RealDataDate);
            this.Controls.Add(this.RealDataSaleType);
            this.Controls.Add(this.label25);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.RealDataType);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.RealDataHeating);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.label24);
            this.Controls.Add(this.RealDataFloor);
            this.Controls.Add(this.RealDataStructure);
            this.Controls.Add(this.label22);
            this.Controls.Add(this.RealDataSurface);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.RealDataPrice);
            this.Name = "RealestateDetailsForm";
            this.Text = "RealestateDetailsForm";
            this.Load += new System.EventHandler(this.RealestateDetailsForm_Load);
            this.Controls.SetChildIndex(this.SaveDetailsForm, 0);
            this.Controls.SetChildIndex(this.CancelDetailsForm, 0);
            this.Controls.SetChildIndex(this.DeleteDetailsForm, 0);
            this.Controls.SetChildIndex(this.RealDataPrice, 0);
            this.Controls.SetChildIndex(this.label11, 0);
            this.Controls.SetChildIndex(this.label14, 0);
            this.Controls.SetChildIndex(this.RealDataSurface, 0);
            this.Controls.SetChildIndex(this.label22, 0);
            this.Controls.SetChildIndex(this.RealDataStructure, 0);
            this.Controls.SetChildIndex(this.RealDataFloor, 0);
            this.Controls.SetChildIndex(this.label24, 0);
            this.Controls.SetChildIndex(this.label15, 0);
            this.Controls.SetChildIndex(this.RealDataHeating, 0);
            this.Controls.SetChildIndex(this.label16, 0);
            this.Controls.SetChildIndex(this.RealDataType, 0);
            this.Controls.SetChildIndex(this.label18, 0);
            this.Controls.SetChildIndex(this.label25, 0);
            this.Controls.SetChildIndex(this.RealDataSaleType, 0);
            this.Controls.SetChildIndex(this.RealDataDate, 0);
            this.Controls.SetChildIndex(this.label26, 0);
            this.Controls.SetChildIndex(this.RealDataStatus, 0);
            this.Controls.SetChildIndex(this.label19, 0);
            this.Controls.SetChildIndex(this.RealDataAgency, 0);
            this.Controls.SetChildIndex(this.RealDataTeras, 0);
            this.Controls.SetChildIndex(this.RealDataBooked, 0);
            this.Controls.SetChildIndex(this.label1, 0);
            this.Controls.SetChildIndex(this.RealDataAgent, 0);
            this.Controls.SetChildIndex(this.button1, 0);
            this.Controls.SetChildIndex(this.button2, 0);
            this.Controls.SetChildIndex(this.RealDataFinancing, 0);
            this.Controls.SetChildIndex(this.RealDataDescription, 0);
            this.Controls.SetChildIndex(this.label4, 0);
            this.Controls.SetChildIndex(this.label5, 0);
            this.Controls.SetChildIndex(this.label6, 0);
            this.Controls.SetChildIndex(this.label2, 0);
            this.Controls.SetChildIndex(this.Activities, 0);
            this.Controls.SetChildIndex(this.label3, 0);
            this.Controls.SetChildIndex(this.Documents, 0);
            this.Controls.SetChildIndex(this.label7, 0);
            this.Controls.SetChildIndex(this.Photos, 0);
            this.Controls.SetChildIndex(this.button3, 0);
            this.Controls.SetChildIndex(this.RealDataFinished, 0);
            this.Controls.SetChildIndex(this.RealDataRooms, 0);
            ((System.ComponentModel.ISupportInitialize)(this.Activities)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Documents)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Photos)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public const string TABLE_NAME = "tbl_realestate";
        public const string COL_PRICE = "price";
        public const string COL_SURFACE = "surface";
        public const string COL_STRUCTURE = "structure";
        public const string COL_FLOOR = "floor";
        public const string COL_ID = "id_realestate";
        public const string COL_HEATING = "heating";
        public const string COL_TERAS = "teras";
        public const string COL_TYPE = "type";
        public const string COL_DATE = "date";
        public const string COL_BOOKED = "booked";
        public const string COL_STATUS = "status";
        public const string COL_SALE_TYPE = "sale_type";
        public const string COL_AGENCY = "agency";
        public const string COL_AGENT = "agent";
        public const string COL_DESCRIPTION = "description";
        public const string COL_ACTIVITIES = "activities";
        public const string COL_FINANCING = "financing";

        private System.Windows.Forms.CheckBox RealDataBooked;
        private System.Windows.Forms.CheckBox RealDataTeras;
        private System.Windows.Forms.ComboBox RealDataAgency;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.ComboBox RealDataStatus;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.DateTimePicker RealDataDate;
        private System.Windows.Forms.ComboBox RealDataSaleType;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.ComboBox RealDataType;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.ComboBox RealDataHeating;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.TextBox RealDataFloor;
        private System.Windows.Forms.ComboBox RealDataStructure;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox RealDataSurface;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox RealDataPrice;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.RichTextBox RealDataFinancing;
        private System.Windows.Forms.RichTextBox RealDataDescription;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;

        public string RealPrice
        {
            get { return RealDataPrice.Text; }
            set { RealDataPrice.Text = value; }
        }

        public string RealSurface
        {
            get { return RealDataSurface.Text; }
            set { RealDataSurface.Text = value; }
        }

        public int RealStructure
        {
            get { return (int)RealDataStructure.SelectedValue; }
            set { RealDataStructure.SelectedValue = value; RealDataStructure.Refresh(); }
        }

        public string RealFloor
        {
            get { return RealDataFloor.Text; }
            set { RealDataFloor.Text = value; }
        }

        public int RealHeating
        {
            get { return (int)RealDataHeating.SelectedValue; }
            set { RealDataHeating.SelectedValue = value; RealDataHeating.Refresh(); }
        }

        public int RealType
        {
            get { return (int)RealDataType.SelectedValue; }
            set { RealDataType.SelectedValue = value; RealDataType.Refresh(); }
        }

        public int RealTeras
        {
            get { return RealDataTeras.Checked ? 1 : 0; }
            set { RealDataTeras.Checked = (value == 1 ? true : false); }
        }

        public string RealDate
        {
            get { return RealDataDate.Text; }
            set { RealDataDate.Text = value; }
        }

        public int RealBooked
        {
            get { return RealDataBooked.Checked ? 1 : 0; }
            set { RealDataBooked.Checked = (value == 1 ? true : false); }
        }

        public int RealStatus
        {
            get { return (int)RealDataStatus.SelectedValue; }
            set { RealDataStatus.SelectedValue = value; RealDataStatus.Refresh(); }
        }

        public int RealSaleType
        {
            get { return (int)RealDataSaleType.SelectedValue; }
            set { RealDataSaleType.SelectedValue = value; RealDataSaleType.Refresh(); }
        }

        public int RealAgency
        {
            get { return (int)RealDataAgency.SelectedValue; }
            set { RealDataAgency.SelectedValue = value; RealDataAgency.Refresh(); }
        }

        public int RealAgent
        {
            get { return (int)RealDataAgent.SelectedValue; }
            set { RealDataAgent.SelectedValue = value; RealDataAgent.Refresh(); }
        }

        public string RealDescription
        {
            get { return RealDataDescription.Text; }
            set { RealDataDescription.Text = value; }
        }

        public string RealFinancing
        {
            get { return RealDataFinancing.Text; }
            set { RealDataFinancing.Text = value; }
        }

        public string RealRooms
        {
            get { return RealDataRooms.Text; }
            set { RealDataRooms.Text = value; }
        }

        public int RealFinished
        {
            get { return RealDataFinished.Checked ? 1 : 0; }
            set { RealDataFinished.Checked = (value == 1 ? true : false); }
        }

        private string _id = null;

        public void reloadAgencies()
        {
            DBManager.ImportInCombo(RealDataAgency, AgencyDetailsForm.TABLE_NAME, AgencyDetailsForm.COL_NAME, AgencyDetailsForm.COL_ID);
        }

        public void reloadAgents()
        {
            DBManager.ImportInCombo(RealDataAgent, UserDetailsForm.TABLE_NAME, UserDetailsForm.COL_USERNAME, UserDetailsForm.COL_ID);
        }
    
        public string RealId
        {
            get { return _id; }
            set { _id = value; }
        }

        public void setLanguage()
        {
            this.label25.Text = CurrentForm.form.ResourceManager.GetString("date");
            this.label11.Text = CurrentForm.form.ResourceManager.GetString("price");
            this.label22.Text = CurrentForm.form.ResourceManager.GetString("surface");
            this.label14.Text = CurrentForm.form.ResourceManager.GetString("structure");
            this.label24.Text = CurrentForm.form.ResourceManager.GetString("floor");
            this.label18.Text = CurrentForm.form.ResourceManager.GetString("sale_type");
            this.label15.Text = CurrentForm.form.ResourceManager.GetString("heating");
            this.label16.Text = CurrentForm.form.ResourceManager.GetString("type");
            this.label26.Text = CurrentForm.form.ResourceManager.GetString("status");
            this.label19.Text = CurrentForm.form.ResourceManager.GetString("agency");
            this.label1.Text = CurrentForm.form.ResourceManager.GetString("agent");
            this.label4.Text = CurrentForm.form.ResourceManager.GetString("description");
            this.label5.Text = CurrentForm.form.ResourceManager.GetString("activity");
            this.label6.Text = CurrentForm.form.ResourceManager.GetString("finansing");
            this.RealDataTeras.Text = CurrentForm.form.ResourceManager.GetString("teras");
            this.RealDataBooked.Text = CurrentForm.form.ResourceManager.GetString("booked");
            this.DeleteDetailsForm.Text = CurrentForm.form.ResourceManager.GetString("delete");
            this.SaveDetailsForm.Text = CurrentForm.form.ResourceManager.GetString("save");
            this.CancelDetailsForm.Text = CurrentForm.form.ResourceManager.GetString("cancel");
            this.Text = CurrentForm.form.ResourceManager.GetString("real_details");

            setConstants();
        }

        public void setConstants()
        {
            DBManager.setHeating(RealDataHeating);
            DBManager.setSaleType(RealDataSaleType);
            DBManager.setStructure(RealDataStructure);
            DBManager.setType(RealDataType);
            DBManager.setStatus(RealDataStatus);
        }

        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.CheckBox RealDataFinished;
        public System.Windows.Forms.DataGridView Activities;
        private System.Windows.Forms.RichTextBox RealDataRooms;
        public System.Windows.Forms.DataGridView Documents;
        public System.Windows.Forms.DataGridView Photos;
        public System.Windows.Forms.ComboBox RealDataAgent;
        public System.Windows.Forms.Button button3;
    }

    public class GenericItem
    {
        public string name = null;
        public int id = -1;

        public GenericItem(string n, int i)
        {
            name = n; id = i;
        }

        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        public int Id
        {
            get { return id; }
            set { id = value; }
        }
    }
}