﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections;

namespace PKIprojekat
{
    public partial class AgencyDetailsForm : DetailsForm
    {
        public AgencyDetailsForm()
        {
            InitializeComponent();
        }

        protected override void SaveDetailsForm_Click(object sender, EventArgs e)
        {
            if (Editing)
            {
                Hashtable hash = new Hashtable();
                hash.Add(COL_NAME, AgencyName);
                hash.Add(COL_ADDRESS, AgencyAddress);
                hash.Add(COL_PHONE, AgencyPhone);
                hash.Add(COL_EMAIL, AgencyEmail);
                DBManager.executeQueryUpdate(TABLE_NAME, hash, COL_ID, AgencyId);
            }
            else
            {
                Hashtable hash = new Hashtable();
                hash.Add(COL_NAME, AgencyName);
                hash.Add(COL_ADDRESS, AgencyAddress);
                hash.Add(COL_PHONE, AgencyPhone);
                hash.Add(COL_EMAIL, AgencyEmail);
                DBManager.executeQueryInsert(TABLE_NAME, hash);
                DBManager.ImportInCombo(CurrentForm.form.UserDataAgency, AgencyDetailsForm.TABLE_NAME, AgencyDetailsForm.COL_NAME, AgencyDetailsForm.COL_ID);
                DBManager.ImportInCombo(CurrentForm.form.RealSearchAgency, AgencyDetailsForm.TABLE_NAME, AgencyDetailsForm.COL_NAME, AgencyDetailsForm.COL_ID);
                DBManager.ImportInCombo(CurrentForm.form.UserSearchAgency, AgencyDetailsForm.TABLE_NAME, AgencyDetailsForm.COL_NAME, AgencyDetailsForm.COL_ID);                
            }
            Dispose();
        }

        protected override void DeleteDetailsForm_Click(object sender, EventArgs e)
        {
            DBManager.executeQueryDelete(TABLE_NAME, COL_ID, AgencyId);
            DBManager.ImportInCombo(this.WindowForm.UserDataAgency, AgencyDetailsForm.TABLE_NAME, AgencyDetailsForm.COL_NAME, AgencyDetailsForm.COL_ID);
            DBManager.ImportInCombo(CurrentForm.form.RealSearchAgency, AgencyDetailsForm.TABLE_NAME, AgencyDetailsForm.COL_NAME, AgencyDetailsForm.COL_ID);
            DBManager.ImportInCombo(CurrentForm.form.UserSearchAgency, AgencyDetailsForm.TABLE_NAME, AgencyDetailsForm.COL_NAME, AgencyDetailsForm.COL_ID);
            Dispose();
        }
    }
}
