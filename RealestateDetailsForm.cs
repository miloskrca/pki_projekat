﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace PKIprojekat
{
    public partial class RealestateDetailsForm : DetailsForm
    {
        public bool same;

        public RealestateDetailsForm()
        {
            InitializeComponent();
        }

        protected override void SaveDetailsForm_Click(object sender, EventArgs e)
        {
            if (Editing)
            {
                Hashtable hash = new Hashtable();
                hash.Add(COL_PRICE, RealPrice);
                hash.Add(COL_SURFACE, RealSurface);
                hash.Add(COL_STRUCTURE, RealStructure);
                hash.Add(COL_FLOOR, RealFloor);
                hash.Add(COL_HEATING, RealHeating);
                hash.Add(COL_TERAS, RealTeras);
                hash.Add(COL_TYPE, RealType);
                hash.Add(COL_DATE, RealDate);
                hash.Add(COL_BOOKED, RealBooked);
                hash.Add(COL_STATUS, RealStatus);
                hash.Add(COL_SALE_TYPE, RealSaleType);
                hash.Add(COL_AGENCY, RealAgency);
                hash.Add(COL_AGENT, RealAgent);
                hash.Add(COL_DESCRIPTION, RealDescription);
                hash.Add(COL_FINANCING, RealFinancing);
                hash.Add("finished", RealFinished);
                hash.Add("rooms", RealRooms);
                DBManager.executeQueryUpdate(TABLE_NAME, hash, COL_ID, RealId);
            }
            else
            {
                Hashtable hash = new Hashtable();
                hash.Add(COL_PRICE, RealPrice);
                hash.Add(COL_SURFACE, RealSurface);
                hash.Add(COL_STRUCTURE, RealStructure);
                hash.Add(COL_FLOOR, RealFloor);
                hash.Add(COL_HEATING, RealHeating);
                hash.Add(COL_TERAS, RealTeras);
                hash.Add(COL_TYPE, RealType);
                hash.Add(COL_DATE, RealDate);
                hash.Add(COL_BOOKED, RealBooked);
                hash.Add(COL_STATUS, RealStatus);
                hash.Add(COL_SALE_TYPE, RealSaleType);
                hash.Add(COL_AGENCY, RealAgency);
                hash.Add(COL_AGENT, RealAgent);
                hash.Add(COL_DESCRIPTION, RealDescription);
                hash.Add(COL_FINANCING, RealFinancing);
                hash.Add("finished", RealFinished);
                hash.Add("rooms", RealRooms);
                DBManager.executeQueryInsert(TABLE_NAME, hash);
            }
            Dispose();
        }

        protected override void DeleteDetailsForm_Click(object sender, EventArgs e)
        {
            DBManager.executeQueryDelete(TABLE_NAME, COL_ID, RealId);
            Dispose();
        }

        private void RealestateDetailsForm_Load(object sender, EventArgs e)
        {
        }

        private void button3_Click(object sender, EventArgs e)
        {
            ActivityForm form = new ActivityForm();
            form.realeastate = int.Parse(RealId);
            form.form = this;
            form.same = same;
            DBManager.ImportInCombo(form.comboBox1, UserDetailsForm.TABLE_NAME, UserDetailsForm.COL_USERNAME, UserDetailsForm.COL_ID);
            form.button3.Visible = false;
            form.comboBox1.SelectedValue = CurrentForm.form.user_id;
            form.comboBox1.Enabled = false;
            form.ShowDialog();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            DocumentForm form = new DocumentForm();
            form.realeastate = int.Parse(RealId);
            form.form = this;
            form.ShowDialog();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            PhotoForm form = new PhotoForm();
            form.realeastate = int.Parse(RealId);
            form.form = this;
            form.ShowDialog();
        }
    }
}
