﻿using System.Globalization;
using System.Resources;
using System.Threading;
using System.Windows.Forms;

namespace PKIprojekat
{
    partial class WindowForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.LogOutMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.languageToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.englishToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.serbianToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.Tabs = new System.Windows.Forms.TabControl();
            this.UserDataTab = new System.Windows.Forms.TabPage();
            this.UserDataId = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.UserDataSave = new System.Windows.Forms.Button();
            this.UserDataAgency = new System.Windows.Forms.ComboBox();
            this.UserDataLastname = new System.Windows.Forms.TextBox();
            this.UserDataName = new System.Windows.Forms.TextBox();
            this.UserDataPassword = new System.Windows.Forms.TextBox();
            this.UserDataUsername = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.UserDataLabelName = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.UsersTab = new System.Windows.Forms.TabPage();
            this.UserSearchResultsPanel = new System.Windows.Forms.Panel();
            this.UserSearchPanel = new System.Windows.Forms.Panel();
            this.UserCreate = new System.Windows.Forms.Button();
            this.UserSearchID = new System.Windows.Forms.TextBox();
            this.UserSearchSearch = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.UserSearchRole = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.UserSearchAgency = new System.Windows.Forms.ComboBox();
            this.bindingSource4 = new System.Windows.Forms.BindingSource(this.components);
            this.agencies = new PKIprojekat.Agencies();
            this.label8 = new System.Windows.Forms.Label();
            this.UserSearchLastname = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.UserSearchName = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.AgencyTab = new System.Windows.Forms.TabPage();
            this.AgencySearchResultsPanel = new System.Windows.Forms.Panel();
            this.AgencySearchPanel = new System.Windows.Forms.Panel();
            this.AgencyCreate = new System.Windows.Forms.Button();
            this.AgencySearchSearch = new System.Windows.Forms.Button();
            this.label12 = new System.Windows.Forms.Label();
            this.AgencySearchName = new System.Windows.Forms.TextBox();
            this.RealestateTab = new System.Windows.Forms.TabPage();
            this.RealestateSearchResultsPanel = new System.Windows.Forms.Panel();
            this.RealestateSearchPanel = new System.Windows.Forms.Panel();
            this.RealestateCreate = new System.Windows.Forms.Button();
            this.RealSearchBooked = new System.Windows.Forms.CheckBox();
            this.RealSearchTeras = new System.Windows.Forms.CheckBox();
            this.RealSearchAgency = new System.Windows.Forms.ComboBox();
            this.label19 = new System.Windows.Forms.Label();
            this.RealSearchStatus = new System.Windows.Forms.ComboBox();
            this.tblrealestatestatusBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.status = new PKIprojekat.Status();
            this.label26 = new System.Windows.Forms.Label();
            this.RealSearchSaleType = new System.Windows.Forms.ComboBox();
            this.tblsaletypeBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.saleType = new PKIprojekat.SaleType();
            this.label18 = new System.Windows.Forms.Label();
            this.RealSearchType = new System.Windows.Forms.ComboBox();
            this.tblrealestatetypeBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.realetateType = new PKIprojekat.RealetateType();
            this.label16 = new System.Windows.Forms.Label();
            this.RealSearchAgent = new System.Windows.Forms.ComboBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.RealSearchFloorTo = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.RealSearchFloorFrom = new System.Windows.Forms.TextBox();
            this.RealSearchStructure = new System.Windows.Forms.ComboBox();
            this.tblstructureBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.structure = new PKIprojekat.Structure();
            this.label13 = new System.Windows.Forms.Label();
            this.RealSearchSurfaceTo = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.RealSearchSurfaceFrom = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.RealSearchPriceTo = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.RealestateSearchSearch = new System.Windows.Forms.Button();
            this.label11 = new System.Windows.Forms.Label();
            this.RealSearchPriceFrom = new System.Windows.Forms.TextBox();
            this.bindingSource3 = new System.Windows.Forms.BindingSource(this.components);
            this.tblusersBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.users = new PKIprojekat.Users();
            this.tblroleBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.roles = new PKIprojekat.Roles();
            this.tblheatingBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.heatingBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.heating = new PKIprojekat.Heating();
            this.databaseDataSet2 = new PKIprojekat.DatabaseDataSet2();
            this.databaseDataSet2BindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.tbl_structureTableAdapter = new PKIprojekat.StructureTableAdapters.tbl_structureTableAdapter();
            this.tbl_heatingTableAdapter = new PKIprojekat.HeatingTableAdapters.tbl_heatingTableAdapter();
            this.tbl_realestate_typeTableAdapter = new PKIprojekat.RealetateTypeTableAdapters.tbl_realestate_typeTableAdapter();
            this.tbl_realestate_statusTableAdapter = new PKIprojekat.StatusTableAdapters.tbl_realestate_statusTableAdapter();
            this.tbl_sale_typeTableAdapter = new PKIprojekat.SaleTypeTableAdapters.tbl_sale_typeTableAdapter();
            this.tbl_agencyTableAdapter = new PKIprojekat.AgenciesTableAdapters.tbl_agencyTableAdapter();
            this.tbl_roleTableAdapter = new PKIprojekat.RolesTableAdapters.tbl_roleTableAdapter();
            this.tbl_usersTableAdapter = new PKIprojekat.UsersTableAdapters.tbl_usersTableAdapter();
            this.menuStrip1.SuspendLayout();
            this.Tabs.SuspendLayout();
            this.UserDataTab.SuspendLayout();
            this.UsersTab.SuspendLayout();
            this.UserSearchPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.agencies)).BeginInit();
            this.AgencyTab.SuspendLayout();
            this.AgencySearchPanel.SuspendLayout();
            this.RealestateTab.SuspendLayout();
            this.RealestateSearchPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tblrealestatestatusBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.status)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblsaletypeBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.saleType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblrealestatetypeBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.realetateType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblstructureBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.structure)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblusersBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.users)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblroleBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.roles)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblheatingBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.heatingBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.heating)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.databaseDataSet2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.databaseDataSet2BindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.editToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(784, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.LogOutMenuItem,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // LogOutMenuItem
            // 
            this.LogOutMenuItem.Name = "LogOutMenuItem";
            this.LogOutMenuItem.Size = new System.Drawing.Size(117, 22);
            this.LogOutMenuItem.Text = "Log Out";
            this.LogOutMenuItem.Click += new System.EventHandler(this.LogOutMenuItem_Click);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(117, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // editToolStripMenuItem
            // 
            this.editToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.languageToolStripMenuItem});
            this.editToolStripMenuItem.Name = "editToolStripMenuItem";
            this.editToolStripMenuItem.Size = new System.Drawing.Size(39, 20);
            this.editToolStripMenuItem.Text = "Edit";
            // 
            // languageToolStripMenuItem
            // 
            this.languageToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.englishToolStripMenuItem,
            this.serbianToolStripMenuItem});
            this.languageToolStripMenuItem.Name = "languageToolStripMenuItem";
            this.languageToolStripMenuItem.Size = new System.Drawing.Size(126, 22);
            this.languageToolStripMenuItem.Text = "Language";
            // 
            // englishToolStripMenuItem
            // 
            this.englishToolStripMenuItem.Name = "englishToolStripMenuItem";
            this.englishToolStripMenuItem.Size = new System.Drawing.Size(113, 22);
            this.englishToolStripMenuItem.Text = "English";
            this.englishToolStripMenuItem.Click += new System.EventHandler(this.englishToolStripMenuItem_Click);
            // 
            // serbianToolStripMenuItem
            // 
            this.serbianToolStripMenuItem.Name = "serbianToolStripMenuItem";
            this.serbianToolStripMenuItem.Size = new System.Drawing.Size(113, 22);
            this.serbianToolStripMenuItem.Text = "Serbian";
            this.serbianToolStripMenuItem.Click += new System.EventHandler(this.serbianToolStripMenuItem_Click);
            // 
            // Tabs
            // 
            this.Tabs.Controls.Add(this.UserDataTab);
            this.Tabs.Controls.Add(this.UsersTab);
            this.Tabs.Controls.Add(this.AgencyTab);
            this.Tabs.Controls.Add(this.RealestateTab);
            this.Tabs.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Tabs.Location = new System.Drawing.Point(0, 24);
            this.Tabs.Multiline = true;
            this.Tabs.Name = "Tabs";
            this.Tabs.SelectedIndex = 0;
            this.Tabs.Size = new System.Drawing.Size(784, 538);
            this.Tabs.TabIndex = 1;
            // 
            // UserDataTab
            // 
            this.UserDataTab.Controls.Add(this.UserDataId);
            this.UserDataTab.Controls.Add(this.label20);
            this.UserDataTab.Controls.Add(this.UserDataSave);
            this.UserDataTab.Controls.Add(this.UserDataAgency);
            this.UserDataTab.Controls.Add(this.UserDataLastname);
            this.UserDataTab.Controls.Add(this.UserDataName);
            this.UserDataTab.Controls.Add(this.UserDataPassword);
            this.UserDataTab.Controls.Add(this.UserDataUsername);
            this.UserDataTab.Controls.Add(this.label5);
            this.UserDataTab.Controls.Add(this.label4);
            this.UserDataTab.Controls.Add(this.UserDataLabelName);
            this.UserDataTab.Controls.Add(this.label2);
            this.UserDataTab.Controls.Add(this.label1);
            this.UserDataTab.Location = new System.Drawing.Point(4, 22);
            this.UserDataTab.Name = "UserDataTab";
            this.UserDataTab.Padding = new System.Windows.Forms.Padding(3);
            this.UserDataTab.Size = new System.Drawing.Size(776, 512);
            this.UserDataTab.TabIndex = 0;
            this.UserDataTab.Text = "Podaci";
            this.UserDataTab.UseVisualStyleBackColor = true;
            // 
            // UserDataId
            // 
            this.UserDataId.AutoSize = true;
            this.UserDataId.Location = new System.Drawing.Point(315, 66);
            this.UserDataId.Name = "UserDataId";
            this.UserDataId.Size = new System.Drawing.Size(10, 13);
            this.UserDataId.TabIndex = 12;
            this.UserDataId.Text = "-";
            // 
            // label20
            // 
            this.label20.Location = new System.Drawing.Point(288, 66);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(21, 13);
            this.label20.TabIndex = 11;
            this.label20.Text = "ID:";
            // 
            // UserDataSave
            // 
            this.UserDataSave.Location = new System.Drawing.Point(365, 369);
            this.UserDataSave.Name = "UserDataSave";
            this.UserDataSave.Size = new System.Drawing.Size(75, 23);
            this.UserDataSave.TabIndex = 10;
            this.UserDataSave.Text = "Sacuvaj";
            this.UserDataSave.UseVisualStyleBackColor = true;
            this.UserDataSave.Click += new System.EventHandler(this.UserDataSave_Click);
            // 
            // UserDataAgency
            // 
            this.UserDataAgency.DisplayMember = "name";
            this.UserDataAgency.FormattingEnabled = true;
            this.UserDataAgency.Location = new System.Drawing.Point(316, 317);
            this.UserDataAgency.Name = "UserDataAgency";
            this.UserDataAgency.Size = new System.Drawing.Size(202, 21);
            this.UserDataAgency.TabIndex = 9;
            this.UserDataAgency.ValueMember = "id_agency";
            // 
            // UserDataLastname
            // 
            this.UserDataLastname.Location = new System.Drawing.Point(315, 268);
            this.UserDataLastname.Name = "UserDataLastname";
            this.UserDataLastname.Size = new System.Drawing.Size(203, 20);
            this.UserDataLastname.TabIndex = 8;
            // 
            // UserDataName
            // 
            this.UserDataName.Location = new System.Drawing.Point(315, 222);
            this.UserDataName.Name = "UserDataName";
            this.UserDataName.Size = new System.Drawing.Size(203, 20);
            this.UserDataName.TabIndex = 7;
            // 
            // UserDataPassword
            // 
            this.UserDataPassword.Location = new System.Drawing.Point(315, 143);
            this.UserDataPassword.Name = "UserDataPassword";
            this.UserDataPassword.PasswordChar = '*';
            this.UserDataPassword.Size = new System.Drawing.Size(203, 20);
            this.UserDataPassword.TabIndex = 6;
            // 
            // UserDataUsername
            // 
            this.UserDataUsername.Enabled = false;
            this.UserDataUsername.Location = new System.Drawing.Point(315, 98);
            this.UserDataUsername.Name = "UserDataUsername";
            this.UserDataUsername.Size = new System.Drawing.Size(203, 20);
            this.UserDataUsername.TabIndex = 5;
            // 
            // label5
            // 
            this.label5.Location = new System.Drawing.Point(229, 320);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(80, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "Agencija:";
            this.label5.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label4.Location = new System.Drawing.Point(229, 271);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(80, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Prezime:";
            this.label4.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // UserDataLabelName
            // 
            this.UserDataLabelName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.UserDataLabelName.Location = new System.Drawing.Point(229, 225);
            this.UserDataLabelName.Name = "UserDataLabelName";
            this.UserDataLabelName.Size = new System.Drawing.Size(80, 13);
            this.UserDataLabelName.TabIndex = 2;
            this.UserDataLabelName.Text = "Ime:";
            this.UserDataLabelName.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.Location = new System.Drawing.Point(230, 146);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(80, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Loinka:";
            this.label2.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.Location = new System.Drawing.Point(230, 101);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(80, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Korisnicko Ime:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // UsersTab
            // 
            this.UsersTab.Controls.Add(this.UserSearchResultsPanel);
            this.UsersTab.Controls.Add(this.UserSearchPanel);
            this.UsersTab.Location = new System.Drawing.Point(4, 22);
            this.UsersTab.Name = "UsersTab";
            this.UsersTab.Size = new System.Drawing.Size(776, 512);
            this.UsersTab.TabIndex = 1;
            this.UsersTab.Text = "Korisnici";
            this.UsersTab.UseVisualStyleBackColor = true;
            // 
            // UserSearchResultsPanel
            // 
            this.UserSearchResultsPanel.BackColor = System.Drawing.Color.DarkGray;
            this.UserSearchResultsPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.UserSearchResultsPanel.Location = new System.Drawing.Point(0, 100);
            this.UserSearchResultsPanel.Name = "UserSearchResultsPanel";
            this.UserSearchResultsPanel.Size = new System.Drawing.Size(776, 412);
            this.UserSearchResultsPanel.TabIndex = 12;
            // 
            // UserSearchPanel
            // 
            this.UserSearchPanel.Controls.Add(this.UserCreate);
            this.UserSearchPanel.Controls.Add(this.UserSearchID);
            this.UserSearchPanel.Controls.Add(this.UserSearchSearch);
            this.UserSearchPanel.Controls.Add(this.label6);
            this.UserSearchPanel.Controls.Add(this.UserSearchRole);
            this.UserSearchPanel.Controls.Add(this.label7);
            this.UserSearchPanel.Controls.Add(this.UserSearchAgency);
            this.UserSearchPanel.Controls.Add(this.label8);
            this.UserSearchPanel.Controls.Add(this.UserSearchLastname);
            this.UserSearchPanel.Controls.Add(this.label9);
            this.UserSearchPanel.Controls.Add(this.UserSearchName);
            this.UserSearchPanel.Controls.Add(this.label10);
            this.UserSearchPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.UserSearchPanel.Location = new System.Drawing.Point(0, 0);
            this.UserSearchPanel.Name = "UserSearchPanel";
            this.UserSearchPanel.Size = new System.Drawing.Size(776, 100);
            this.UserSearchPanel.TabIndex = 11;
            // 
            // UserCreate
            // 
            this.UserCreate.Location = new System.Drawing.Point(655, 8);
            this.UserCreate.Name = "UserCreate";
            this.UserCreate.Size = new System.Drawing.Size(110, 23);
            this.UserCreate.TabIndex = 60;
            this.UserCreate.Text = "Napravi";
            this.UserCreate.UseVisualStyleBackColor = true;
            this.UserCreate.Click += new System.EventHandler(this.UserCreate_Click);
            // 
            // UserSearchID
            // 
            this.UserSearchID.Location = new System.Drawing.Point(36, 16);
            this.UserSearchID.Name = "UserSearchID";
            this.UserSearchID.Size = new System.Drawing.Size(100, 20);
            this.UserSearchID.TabIndex = 5;
            // 
            // UserSearchSearch
            // 
            this.UserSearchSearch.Location = new System.Drawing.Point(655, 37);
            this.UserSearchSearch.Name = "UserSearchSearch";
            this.UserSearchSearch.Size = new System.Drawing.Size(110, 23);
            this.UserSearchSearch.TabIndex = 10;
            this.UserSearchSearch.Text = "Pretrazi";
            this.UserSearchSearch.UseVisualStyleBackColor = true;
            this.UserSearchSearch.Click += new System.EventHandler(this.UserSearchSearch_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(9, 19);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(21, 13);
            this.label6.TabIndex = 0;
            this.label6.Text = "ID:";
            // 
            // UserSearchRole
            // 
            this.UserSearchRole.DisplayMember = "name";
            this.UserSearchRole.FormattingEnabled = true;
            this.UserSearchRole.Location = new System.Drawing.Point(472, 43);
            this.UserSearchRole.Name = "UserSearchRole";
            this.UserSearchRole.Size = new System.Drawing.Size(163, 21);
            this.UserSearchRole.TabIndex = 9;
            this.UserSearchRole.ValueMember = "id_role";
            // 
            // label7
            // 
            this.label7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label7.Location = new System.Drawing.Point(159, 19);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(50, 13);
            this.label7.TabIndex = 1;
            this.label7.Text = "Ime:";
            this.label7.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // UserSearchAgency
            // 
            this.UserSearchAgency.DataSource = this.bindingSource4;
            this.UserSearchAgency.DisplayMember = "name";
            this.UserSearchAgency.FormattingEnabled = true;
            this.UserSearchAgency.Location = new System.Drawing.Point(472, 16);
            this.UserSearchAgency.Name = "UserSearchAgency";
            this.UserSearchAgency.Size = new System.Drawing.Size(163, 21);
            this.UserSearchAgency.TabIndex = 8;
            this.UserSearchAgency.ValueMember = "id_agency";
            // 
            // bindingSource4
            // 
            this.bindingSource4.DataMember = "tbl_agency";
            this.bindingSource4.DataSource = this.agencies;
            // 
            // agencies
            // 
            this.agencies.DataSetName = "Agencies";
            this.agencies.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // label8
            // 
            this.label8.Location = new System.Drawing.Point(159, 45);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(50, 13);
            this.label8.TabIndex = 2;
            this.label8.Text = "Prezime:";
            this.label8.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // UserSearchLastname
            // 
            this.UserSearchLastname.Location = new System.Drawing.Point(212, 42);
            this.UserSearchLastname.Name = "UserSearchLastname";
            this.UserSearchLastname.Size = new System.Drawing.Size(178, 20);
            this.UserSearchLastname.TabIndex = 7;
            // 
            // label9
            // 
            this.label9.Location = new System.Drawing.Point(406, 19);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(60, 13);
            this.label9.TabIndex = 3;
            this.label9.Text = "Agencija:";
            this.label9.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // UserSearchName
            // 
            this.UserSearchName.Location = new System.Drawing.Point(212, 16);
            this.UserSearchName.Name = "UserSearchName";
            this.UserSearchName.Size = new System.Drawing.Size(178, 20);
            this.UserSearchName.TabIndex = 6;
            // 
            // label10
            // 
            this.label10.Location = new System.Drawing.Point(406, 46);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(60, 13);
            this.label10.TabIndex = 4;
            this.label10.Text = "Uloga:";
            this.label10.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // AgencyTab
            // 
            this.AgencyTab.Controls.Add(this.AgencySearchResultsPanel);
            this.AgencyTab.Controls.Add(this.AgencySearchPanel);
            this.AgencyTab.Location = new System.Drawing.Point(4, 22);
            this.AgencyTab.Name = "AgencyTab";
            this.AgencyTab.Size = new System.Drawing.Size(776, 512);
            this.AgencyTab.TabIndex = 2;
            this.AgencyTab.Text = "Agencije";
            this.AgencyTab.UseVisualStyleBackColor = true;
            // 
            // AgencySearchResultsPanel
            // 
            this.AgencySearchResultsPanel.BackColor = System.Drawing.Color.DarkGray;
            this.AgencySearchResultsPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.AgencySearchResultsPanel.Location = new System.Drawing.Point(0, 100);
            this.AgencySearchResultsPanel.Name = "AgencySearchResultsPanel";
            this.AgencySearchResultsPanel.Size = new System.Drawing.Size(776, 412);
            this.AgencySearchResultsPanel.TabIndex = 14;
            // 
            // AgencySearchPanel
            // 
            this.AgencySearchPanel.Controls.Add(this.AgencyCreate);
            this.AgencySearchPanel.Controls.Add(this.AgencySearchSearch);
            this.AgencySearchPanel.Controls.Add(this.label12);
            this.AgencySearchPanel.Controls.Add(this.AgencySearchName);
            this.AgencySearchPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.AgencySearchPanel.Location = new System.Drawing.Point(0, 0);
            this.AgencySearchPanel.Name = "AgencySearchPanel";
            this.AgencySearchPanel.Size = new System.Drawing.Size(776, 100);
            this.AgencySearchPanel.TabIndex = 13;
            // 
            // AgencyCreate
            // 
            this.AgencyCreate.Location = new System.Drawing.Point(655, 8);
            this.AgencyCreate.Name = "AgencyCreate";
            this.AgencyCreate.Size = new System.Drawing.Size(110, 23);
            this.AgencyCreate.TabIndex = 60;
            this.AgencyCreate.Text = "Napravi";
            this.AgencyCreate.UseVisualStyleBackColor = true;
            this.AgencyCreate.Click += new System.EventHandler(this.AgencyCreate_Click);
            // 
            // AgencySearchSearch
            // 
            this.AgencySearchSearch.Location = new System.Drawing.Point(655, 37);
            this.AgencySearchSearch.Name = "AgencySearchSearch";
            this.AgencySearchSearch.Size = new System.Drawing.Size(110, 23);
            this.AgencySearchSearch.TabIndex = 10;
            this.AgencySearchSearch.Text = "Pretrazi";
            this.AgencySearchSearch.UseVisualStyleBackColor = true;
            this.AgencySearchSearch.Click += new System.EventHandler(this.AgencySearchSearch_Click);
            // 
            // label12
            // 
            this.label12.Location = new System.Drawing.Point(156, 19);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(50, 13);
            this.label12.TabIndex = 1;
            this.label12.Text = "Ime:";
            this.label12.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // AgencySearchName
            // 
            this.AgencySearchName.Location = new System.Drawing.Point(212, 16);
            this.AgencySearchName.Name = "AgencySearchName";
            this.AgencySearchName.Size = new System.Drawing.Size(178, 20);
            this.AgencySearchName.TabIndex = 6;
            // 
            // RealestateTab
            // 
            this.RealestateTab.Controls.Add(this.RealestateSearchResultsPanel);
            this.RealestateTab.Controls.Add(this.RealestateSearchPanel);
            this.RealestateTab.Location = new System.Drawing.Point(4, 22);
            this.RealestateTab.Name = "RealestateTab";
            this.RealestateTab.Size = new System.Drawing.Size(776, 512);
            this.RealestateTab.TabIndex = 3;
            this.RealestateTab.Text = "Nekretine";
            this.RealestateTab.UseVisualStyleBackColor = true;
            // 
            // RealestateSearchResultsPanel
            // 
            this.RealestateSearchResultsPanel.BackColor = System.Drawing.Color.DarkGray;
            this.RealestateSearchResultsPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.RealestateSearchResultsPanel.Location = new System.Drawing.Point(0, 118);
            this.RealestateSearchResultsPanel.Name = "RealestateSearchResultsPanel";
            this.RealestateSearchResultsPanel.Size = new System.Drawing.Size(776, 394);
            this.RealestateSearchResultsPanel.TabIndex = 16;
            // 
            // RealestateSearchPanel
            // 
            this.RealestateSearchPanel.Controls.Add(this.RealestateCreate);
            this.RealestateSearchPanel.Controls.Add(this.RealSearchBooked);
            this.RealestateSearchPanel.Controls.Add(this.RealSearchTeras);
            this.RealestateSearchPanel.Controls.Add(this.RealSearchAgency);
            this.RealestateSearchPanel.Controls.Add(this.label19);
            this.RealestateSearchPanel.Controls.Add(this.RealSearchStatus);
            this.RealestateSearchPanel.Controls.Add(this.label26);
            this.RealestateSearchPanel.Controls.Add(this.RealSearchSaleType);
            this.RealestateSearchPanel.Controls.Add(this.label18);
            this.RealestateSearchPanel.Controls.Add(this.RealSearchType);
            this.RealestateSearchPanel.Controls.Add(this.label16);
            this.RealestateSearchPanel.Controls.Add(this.RealSearchAgent);
            this.RealestateSearchPanel.Controls.Add(this.label15);
            this.RealestateSearchPanel.Controls.Add(this.label23);
            this.RealestateSearchPanel.Controls.Add(this.RealSearchFloorTo);
            this.RealestateSearchPanel.Controls.Add(this.label24);
            this.RealestateSearchPanel.Controls.Add(this.RealSearchFloorFrom);
            this.RealestateSearchPanel.Controls.Add(this.RealSearchStructure);
            this.RealestateSearchPanel.Controls.Add(this.label13);
            this.RealestateSearchPanel.Controls.Add(this.RealSearchSurfaceTo);
            this.RealestateSearchPanel.Controls.Add(this.label22);
            this.RealestateSearchPanel.Controls.Add(this.RealSearchSurfaceFrom);
            this.RealestateSearchPanel.Controls.Add(this.label21);
            this.RealestateSearchPanel.Controls.Add(this.RealSearchPriceTo);
            this.RealestateSearchPanel.Controls.Add(this.label14);
            this.RealestateSearchPanel.Controls.Add(this.RealestateSearchSearch);
            this.RealestateSearchPanel.Controls.Add(this.label11);
            this.RealestateSearchPanel.Controls.Add(this.RealSearchPriceFrom);
            this.RealestateSearchPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.RealestateSearchPanel.Location = new System.Drawing.Point(0, 0);
            this.RealestateSearchPanel.Name = "RealestateSearchPanel";
            this.RealestateSearchPanel.Size = new System.Drawing.Size(776, 118);
            this.RealestateSearchPanel.TabIndex = 15;
            // 
            // RealestateCreate
            // 
            this.RealestateCreate.Location = new System.Drawing.Point(655, 8);
            this.RealestateCreate.Name = "RealestateCreate";
            this.RealestateCreate.Size = new System.Drawing.Size(110, 23);
            this.RealestateCreate.TabIndex = 59;
            this.RealestateCreate.Text = "Napravi";
            this.RealestateCreate.UseVisualStyleBackColor = true;
            this.RealestateCreate.Click += new System.EventHandler(this.RealestateCreate_Click);
            // 
            // RealSearchBooked
            // 
            this.RealSearchBooked.AutoSize = true;
            this.RealSearchBooked.Location = new System.Drawing.Point(303, 88);
            this.RealSearchBooked.Name = "RealSearchBooked";
            this.RealSearchBooked.Size = new System.Drawing.Size(73, 17);
            this.RealSearchBooked.TabIndex = 58;
            this.RealSearchBooked.Text = "Uknjizeno";
            this.RealSearchBooked.UseVisualStyleBackColor = true;
            // 
            // RealSearchTeras
            // 
            this.RealSearchTeras.AutoSize = true;
            this.RealSearchTeras.Location = new System.Drawing.Point(303, 65);
            this.RealSearchTeras.Name = "RealSearchTeras";
            this.RealSearchTeras.Size = new System.Drawing.Size(59, 17);
            this.RealSearchTeras.TabIndex = 57;
            this.RealSearchTeras.Text = "Terasa";
            this.RealSearchTeras.UseVisualStyleBackColor = true;
            // 
            // RealSearchAgency
            // 
            this.RealSearchAgency.FormattingEnabled = true;
            this.RealSearchAgency.Location = new System.Drawing.Point(503, 59);
            this.RealSearchAgency.Name = "RealSearchAgency";
            this.RealSearchAgency.Size = new System.Drawing.Size(137, 21);
            this.RealSearchAgency.TabIndex = 54;
            // 
            // label19
            // 
            this.label19.Location = new System.Drawing.Point(442, 62);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(55, 13);
            this.label19.TabIndex = 53;
            this.label19.Text = "Agencija:";
            this.label19.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // RealSearchStatus
            // 
            this.RealSearchStatus.DataSource = this.tblrealestatestatusBindingSource;
            this.RealSearchStatus.DisplayMember = "name";
            this.RealSearchStatus.FormattingEnabled = true;
            this.RealSearchStatus.Location = new System.Drawing.Point(503, 33);
            this.RealSearchStatus.Name = "RealSearchStatus";
            this.RealSearchStatus.Size = new System.Drawing.Size(137, 21);
            this.RealSearchStatus.TabIndex = 50;
            this.RealSearchStatus.ValueMember = "id_realestate_status";
            // 
            // tblrealestatestatusBindingSource
            // 
            this.tblrealestatestatusBindingSource.DataMember = "tbl_realestate_status";
            this.tblrealestatestatusBindingSource.DataSource = this.status;
            // 
            // status
            // 
            this.status.DataSetName = "Status";
            this.status.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // label26
            // 
            this.label26.Location = new System.Drawing.Point(446, 36);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(55, 13);
            this.label26.TabIndex = 49;
            this.label26.Text = "Status:";
            this.label26.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // RealSearchSaleType
            // 
            this.RealSearchSaleType.DataSource = this.tblsaletypeBindingSource;
            this.RealSearchSaleType.DisplayMember = "name";
            this.RealSearchSaleType.FormattingEnabled = true;
            this.RealSearchSaleType.Location = new System.Drawing.Point(73, 60);
            this.RealSearchSaleType.Name = "RealSearchSaleType";
            this.RealSearchSaleType.Size = new System.Drawing.Size(183, 21);
            this.RealSearchSaleType.TabIndex = 52;
            this.RealSearchSaleType.ValueMember = "id_sale_type";
            // 
            // tblsaletypeBindingSource
            // 
            this.tblsaletypeBindingSource.DataMember = "tbl_sale_type";
            this.tblsaletypeBindingSource.DataSource = this.saleType;
            // 
            // saleType
            // 
            this.saleType.DataSetName = "SaleType";
            this.saleType.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // label18
            // 
            this.label18.Location = new System.Drawing.Point(5, 63);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(63, 13);
            this.label18.TabIndex = 51;
            this.label18.Text = "Tip prodaje:";
            this.label18.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // RealSearchType
            // 
            this.RealSearchType.DataSource = this.tblrealestatetypeBindingSource;
            this.RealSearchType.DisplayMember = "name";
            this.RealSearchType.FormattingEnabled = true;
            this.RealSearchType.Location = new System.Drawing.Point(303, 32);
            this.RealSearchType.Name = "RealSearchType";
            this.RealSearchType.Size = new System.Drawing.Size(137, 21);
            this.RealSearchType.TabIndex = 41;
            this.RealSearchType.ValueMember = "id_realestate_type";
            // 
            // tblrealestatetypeBindingSource
            // 
            this.tblrealestatetypeBindingSource.DataMember = "tbl_realestate_type";
            this.tblrealestatetypeBindingSource.DataSource = this.realetateType;
            // 
            // realetateType
            // 
            this.realetateType.DataSetName = "RealetateType";
            this.realetateType.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // label16
            // 
            this.label16.Location = new System.Drawing.Point(262, 35);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(40, 13);
            this.label16.TabIndex = 40;
            this.label16.Text = "Tip:";
            this.label16.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // RealSearchAgent
            // 
            this.RealSearchAgent.FormattingEnabled = true;
            this.RealSearchAgent.Location = new System.Drawing.Point(303, 6);
            this.RealSearchAgent.Name = "RealSearchAgent";
            this.RealSearchAgent.Size = new System.Drawing.Size(137, 21);
            this.RealSearchAgent.TabIndex = 39;
            // 
            // label15
            // 
            this.label15.Location = new System.Drawing.Point(259, 9);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(40, 13);
            this.label15.TabIndex = 38;
            this.label15.Text = "Agent:";
            this.label15.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(160, 90);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(19, 13);
            this.label23.TabIndex = 36;
            this.label23.Text = "do";
            // 
            // RealSearchFloorTo
            // 
            this.RealSearchFloorTo.Location = new System.Drawing.Point(185, 87);
            this.RealSearchFloorTo.Name = "RealSearchFloorTo";
            this.RealSearchFloorTo.Size = new System.Drawing.Size(70, 20);
            this.RealSearchFloorTo.TabIndex = 37;
            // 
            // label24
            // 
            this.label24.Location = new System.Drawing.Point(8, 89);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(63, 13);
            this.label24.TabIndex = 34;
            this.label24.Text = "Sprat:";
            this.label24.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // RealSearchFloorFrom
            // 
            this.RealSearchFloorFrom.Location = new System.Drawing.Point(73, 86);
            this.RealSearchFloorFrom.Name = "RealSearchFloorFrom";
            this.RealSearchFloorFrom.Size = new System.Drawing.Size(81, 20);
            this.RealSearchFloorFrom.TabIndex = 35;
            // 
            // RealSearchStructure
            // 
            this.RealSearchStructure.DataSource = this.tblstructureBindingSource;
            this.RealSearchStructure.DisplayMember = "name";
            this.RealSearchStructure.FormattingEnabled = true;
            this.RealSearchStructure.Location = new System.Drawing.Point(503, 6);
            this.RealSearchStructure.Name = "RealSearchStructure";
            this.RealSearchStructure.Size = new System.Drawing.Size(137, 21);
            this.RealSearchStructure.TabIndex = 33;
            this.RealSearchStructure.ValueMember = "id_structure";
            // 
            // tblstructureBindingSource
            // 
            this.tblstructureBindingSource.DataMember = "tbl_structure";
            this.tblstructureBindingSource.DataSource = this.structure;
            // 
            // structure
            // 
            this.structure.DataSetName = "Structure";
            this.structure.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(160, 33);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(19, 13);
            this.label13.TabIndex = 31;
            this.label13.Text = "do";
            // 
            // RealSearchSurfaceTo
            // 
            this.RealSearchSurfaceTo.Location = new System.Drawing.Point(185, 32);
            this.RealSearchSurfaceTo.Name = "RealSearchSurfaceTo";
            this.RealSearchSurfaceTo.Size = new System.Drawing.Size(71, 20);
            this.RealSearchSurfaceTo.TabIndex = 32;
            // 
            // label22
            // 
            this.label22.Location = new System.Drawing.Point(5, 35);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(63, 13);
            this.label22.TabIndex = 29;
            this.label22.Text = "Povrsina:";
            this.label22.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // RealSearchSurfaceFrom
            // 
            this.RealSearchSurfaceFrom.Location = new System.Drawing.Point(73, 32);
            this.RealSearchSurfaceFrom.Name = "RealSearchSurfaceFrom";
            this.RealSearchSurfaceFrom.Size = new System.Drawing.Size(81, 20);
            this.RealSearchSurfaceFrom.TabIndex = 30;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(160, 9);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(19, 13);
            this.label21.TabIndex = 27;
            this.label21.Text = "do";
            // 
            // RealSearchPriceTo
            // 
            this.RealSearchPriceTo.Location = new System.Drawing.Point(185, 6);
            this.RealSearchPriceTo.Name = "RealSearchPriceTo";
            this.RealSearchPriceTo.Size = new System.Drawing.Size(70, 20);
            this.RealSearchPriceTo.TabIndex = 28;
            // 
            // label14
            // 
            this.label14.Location = new System.Drawing.Point(446, 9);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(55, 13);
            this.label14.TabIndex = 13;
            this.label14.Text = "Struktura:";
            this.label14.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // RealestateSearchSearch
            // 
            this.RealestateSearchSearch.Location = new System.Drawing.Point(655, 37);
            this.RealestateSearchSearch.Name = "RealestateSearchSearch";
            this.RealestateSearchSearch.Size = new System.Drawing.Size(110, 23);
            this.RealestateSearchSearch.TabIndex = 10;
            this.RealestateSearchSearch.Text = "Pretrazi";
            this.RealestateSearchSearch.UseVisualStyleBackColor = true;
            this.RealestateSearchSearch.Click += new System.EventHandler(this.RealestateSearchSearch_Click);
            // 
            // label11
            // 
            this.label11.Location = new System.Drawing.Point(5, 9);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(63, 13);
            this.label11.TabIndex = 1;
            this.label11.Text = "Cena:";
            this.label11.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // RealSearchPriceFrom
            // 
            this.RealSearchPriceFrom.Location = new System.Drawing.Point(73, 6);
            this.RealSearchPriceFrom.Name = "RealSearchPriceFrom";
            this.RealSearchPriceFrom.Size = new System.Drawing.Size(81, 20);
            this.RealSearchPriceFrom.TabIndex = 6;
            // 
            // bindingSource3
            // 
            this.bindingSource3.DataMember = "tbl_agency";
            this.bindingSource3.DataSource = this.agencies;
            // 
            // tblusersBindingSource
            // 
            this.tblusersBindingSource.DataMember = "tbl_users";
            this.tblusersBindingSource.DataSource = this.users;
            // 
            // users
            // 
            this.users.DataSetName = "Users";
            this.users.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // tblroleBindingSource
            // 
            this.tblroleBindingSource.DataMember = "tbl_role";
            this.tblroleBindingSource.DataSource = this.roles;
            // 
            // roles
            // 
            this.roles.DataSetName = "Roles";
            this.roles.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // tblheatingBindingSource
            // 
            this.tblheatingBindingSource.DataMember = "tbl_heating";
            this.tblheatingBindingSource.DataSource = this.heatingBindingSource;
            // 
            // heatingBindingSource
            // 
            this.heatingBindingSource.DataSource = this.heating;
            this.heatingBindingSource.Position = 0;
            // 
            // heating
            // 
            this.heating.DataSetName = "Heating";
            this.heating.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // databaseDataSet2
            // 
            this.databaseDataSet2.DataSetName = "DatabaseDataSet2";
            this.databaseDataSet2.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // databaseDataSet2BindingSource
            // 
            this.databaseDataSet2BindingSource.DataSource = this.databaseDataSet2;
            this.databaseDataSet2BindingSource.Position = 0;
            // 
            // tbl_structureTableAdapter
            // 
            this.tbl_structureTableAdapter.ClearBeforeFill = true;
            // 
            // tbl_heatingTableAdapter
            // 
            this.tbl_heatingTableAdapter.ClearBeforeFill = true;
            // 
            // tbl_realestate_typeTableAdapter
            // 
            this.tbl_realestate_typeTableAdapter.ClearBeforeFill = true;
            // 
            // tbl_realestate_statusTableAdapter
            // 
            this.tbl_realestate_statusTableAdapter.ClearBeforeFill = true;
            // 
            // tbl_sale_typeTableAdapter
            // 
            this.tbl_sale_typeTableAdapter.ClearBeforeFill = true;
            // 
            // tbl_agencyTableAdapter
            // 
            this.tbl_agencyTableAdapter.ClearBeforeFill = true;
            // 
            // tbl_roleTableAdapter
            // 
            this.tbl_roleTableAdapter.ClearBeforeFill = true;
            // 
            // tbl_usersTableAdapter
            // 
            this.tbl_usersTableAdapter.ClearBeforeFill = true;
            // 
            // WindowForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 562);
            this.Controls.Add(this.Tabs);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "WindowForm";
            this.Text = "PKI projekat";
            this.Load += new System.EventHandler(this.WindowForm_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.Tabs.ResumeLayout(false);
            this.UserDataTab.ResumeLayout(false);
            this.UserDataTab.PerformLayout();
            this.UsersTab.ResumeLayout(false);
            this.UserSearchPanel.ResumeLayout(false);
            this.UserSearchPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.agencies)).EndInit();
            this.AgencyTab.ResumeLayout(false);
            this.AgencySearchPanel.ResumeLayout(false);
            this.AgencySearchPanel.PerformLayout();
            this.RealestateTab.ResumeLayout(false);
            this.RealestateSearchPanel.ResumeLayout(false);
            this.RealestateSearchPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tblrealestatestatusBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.status)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblsaletypeBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.saleType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblrealestatetypeBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.realetateType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblstructureBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.structure)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblusersBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.users)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblroleBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.roles)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblheatingBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.heatingBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.heating)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.databaseDataSet2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.databaseDataSet2BindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.TabControl Tabs;
        private System.Windows.Forms.TabPage UserDataTab;
        public System.Windows.Forms.ComboBox UserDataAgency;
        private System.Windows.Forms.TextBox UserDataLastname;
        private System.Windows.Forms.TextBox UserDataName;
        private System.Windows.Forms.TextBox UserDataPassword;
        private System.Windows.Forms.TextBox UserDataUsername;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label UserDataLabelName;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button UserDataSave;
        private System.Windows.Forms.TabPage UsersTab;
        private System.Windows.Forms.Button UserSearchSearch;
        private System.Windows.Forms.ComboBox UserSearchRole;
        public System.Windows.Forms.ComboBox UserSearchAgency;
        private System.Windows.Forms.TextBox UserSearchLastname;
        private System.Windows.Forms.TextBox UserSearchName;
        private System.Windows.Forms.TextBox UserSearchID;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Panel UserSearchResultsPanel;
        private System.Windows.Forms.Panel UserSearchPanel;
        private System.Windows.Forms.TabPage AgencyTab;
        private System.Windows.Forms.Panel AgencySearchResultsPanel;
        private System.Windows.Forms.Panel AgencySearchPanel;
        private System.Windows.Forms.Button AgencySearchSearch;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox AgencySearchName;
        private System.Windows.Forms.TabPage RealestateTab;
        private System.Windows.Forms.Panel RealestateSearchResultsPanel;
        private System.Windows.Forms.Panel RealestateSearchPanel;
        private System.Windows.Forms.Button RealestateSearchSearch;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox RealSearchPriceFrom;
        private System.Windows.Forms.CheckBox RealSearchBooked;
        private System.Windows.Forms.CheckBox RealSearchTeras;
        public System.Windows.Forms.ComboBox RealSearchAgency;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.ComboBox RealSearchStatus;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.ComboBox RealSearchSaleType;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.ComboBox RealSearchType;
        private System.Windows.Forms.Label label16;
        public System.Windows.Forms.ComboBox RealSearchAgent;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TextBox RealSearchFloorTo;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.TextBox RealSearchFloorFrom;
        private System.Windows.Forms.ComboBox RealSearchStructure;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox RealSearchSurfaceTo;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox RealSearchSurfaceFrom;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox RealSearchPriceTo;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Button UserCreate;
        private System.Windows.Forms.Button AgencyCreate;
        private System.Windows.Forms.Label UserDataId;
        private System.Windows.Forms.Label label20;

        public string DataName
        {
            get { return UserDataName.Text; }
            set { UserDataName.Text = value; }
        }

        public string DataLastname
        {
            get { return UserDataLastname.Text; }
            set { UserDataLastname.Text = value; }
        }

        public string DataUsername
        {
            get { return UserDataUsername.Text; }
            set { UserDataUsername.Text = value; }
        }

        public string DataPassword
        {
            get { return UserDataPassword.Text; }
            set { UserDataPassword.Text = value; }
        }

        public int DataAgency
        {
            get { return (int)UserDataAgency.SelectedValue; }
            set { UserDataAgency.SelectedValue = value; UserDataAgency.Refresh(); }
        }

        public string DataId
        {
            get { return UserDataId.Text; }
            set { UserDataId.Text = value; }
        }

        public void EnableUsername(bool b)
        {
            UserDataUsername.Enabled = b;
        }

        private System.Windows.Forms.ToolStripMenuItem LogOutMenuItem;
        private System.Windows.Forms.BindingSource databaseDataSet1BindingSource;
        private System.Windows.Forms.BindingSource tblagencyBindingSource;
        private System.Windows.Forms.BindingSource databaseDataSetBindingSource;
        private System.Windows.Forms.BindingSource agenciesBindingSource;
        private System.Windows.Forms.BindingSource tblagencyBindingSource1;
        private System.Windows.Forms.BindingSource agenciesBindingSource1;
        private System.Windows.Forms.BindingSource tblagencyBindingSource2;
        private System.Windows.Forms.BindingSource agenciesBindingSource2;
        private System.Windows.Forms.BindingSource bindingSource1;
        private System.Windows.Forms.BindingSource bindingSource2;
        private System.Windows.Forms.BindingSource databaseDataSet2BindingSource;
        private DatabaseDataSet2 databaseDataSet2;
        private Structure structure;
        private System.Windows.Forms.BindingSource tblstructureBindingSource;
        private StructureTableAdapters.tbl_structureTableAdapter tbl_structureTableAdapter;
        private System.Windows.Forms.BindingSource heatingBindingSource;
        private Heating heating;
        private System.Windows.Forms.BindingSource tblheatingBindingSource;
        private HeatingTableAdapters.tbl_heatingTableAdapter tbl_heatingTableAdapter;
        private RealetateType realetateType;
        private System.Windows.Forms.BindingSource tblrealestatetypeBindingSource;
        private RealetateTypeTableAdapters.tbl_realestate_typeTableAdapter tbl_realestate_typeTableAdapter;
        private Status status;
        private System.Windows.Forms.BindingSource tblrealestatestatusBindingSource;
        private StatusTableAdapters.tbl_realestate_statusTableAdapter tbl_realestate_statusTableAdapter;
        private SaleType saleType;
        private System.Windows.Forms.BindingSource tblsaletypeBindingSource;
        private SaleTypeTableAdapters.tbl_sale_typeTableAdapter tbl_sale_typeTableAdapter;
        private Agencies agencies;
        private System.Windows.Forms.BindingSource bindingSource3;
        private AgenciesTableAdapters.tbl_agencyTableAdapter tbl_agencyTableAdapter;
        private System.Windows.Forms.BindingSource bindingSource4;
        private Roles roles;
        private System.Windows.Forms.BindingSource tblroleBindingSource;
        private RolesTableAdapters.tbl_roleTableAdapter tbl_roleTableAdapter;
        private Users users;
        private System.Windows.Forms.BindingSource tblusersBindingSource;
        private UsersTableAdapters.tbl_usersTableAdapter tbl_usersTableAdapter;

        public void setLanguage(int lang)
        {
                if (lang == 1)
                {
                    languageToolStripMenuItem.DropDownItems[0].Enabled = false;
                    languageToolStripMenuItem.DropDownItems[1].Enabled = true;
                    Thread.CurrentThread.CurrentUICulture = Thread.CurrentThread.CurrentCulture;
                    CurrentForm.form.ResourceManager = new ResourceManager("PKIprojekat.Resource.en-EN", System.Reflection.Assembly.GetExecutingAssembly());
                }
                if (lang == 0)
                {
                    languageToolStripMenuItem.DropDownItems[0].Enabled = true;
                    languageToolStripMenuItem.DropDownItems[1].Enabled = false;
                    Thread.CurrentThread.CurrentUICulture = Thread.CurrentThread.CurrentCulture;
                    CurrentForm.form.ResourceManager = new ResourceManager("PKIprojekat.Resource.sr-SR", System.Reflection.Assembly.GetExecutingAssembly());
                }

                this.label2.Text = ResourceManager.GetString("password");
                this.label5.Text = ResourceManager.GetString("agency");
                this.label1.Text = ResourceManager.GetString("username");
                this.label6.Text = ResourceManager.GetString("id");
                this.label20.Text = ResourceManager.GetString("id");
                this.label4.Text = ResourceManager.GetString("lastname");
                this.AgencyTab.Text = ResourceManager.GetString("agencies");
                this.label9.Text = ResourceManager.GetString("agency");
                this.RealestateTab.Text = ResourceManager.GetString("realestate");
                this.AgencySearchSearch.Text = ResourceManager.GetString("search");
                this.label19.Text = ResourceManager.GetString("agency");
                this.label14.Text = ResourceManager.GetString("structure");
                this.label26.Text = ResourceManager.GetString("status");
                this.RealSearchTeras.Text = ResourceManager.GetString("teras");
                this.label15.Text = ResourceManager.GetString("agent");
                this.UsersTab.Text = ResourceManager.GetString("users");
                this.label24.Text = ResourceManager.GetString("floor");
                this.label21.Text = ResourceManager.GetString("to");
                this.RealSearchBooked.Text = ResourceManager.GetString("booked");
                this.label10.Text = ResourceManager.GetString("role");
                this.label12.Text = ResourceManager.GetString("name");
                this.label16.Text = ResourceManager.GetString("type");
                this.label22.Text = ResourceManager.GetString("surface");
                this.label23.Text = ResourceManager.GetString("to");
                this.label13.Text = ResourceManager.GetString("to");
                this.label18.Text = ResourceManager.GetString("sale_type");
                this.UserDataLabelName.Text = ResourceManager.GetString("name");
                this.UserCreate.Text = ResourceManager.GetString("create");
                this.UserSearchSearch.Text = ResourceManager.GetString("search");
                this.label7.Text = ResourceManager.GetString("name");
                this.label8.Text = ResourceManager.GetString("lastname");
                this.AgencyCreate.Text = ResourceManager.GetString("create");
                this.RealestateCreate.Text = ResourceManager.GetString("create");
                this.label11.Text = ResourceManager.GetString("price");
                this.RealestateSearchSearch.Text = ResourceManager.GetString("search");
                this.UserDataTab.Text = ResourceManager.GetString("data");
                this.UserDataSave.Text = ResourceManager.GetString("save");
                setConstants();
        }

        public void setConstants()
        {
            DBManager.setSaleType(RealSearchSaleType);
            DBManager.setStructure(RealSearchStructure);
            DBManager.setType(RealSearchType);
            DBManager.setStatus(RealSearchStatus);
            DBManager.setRole(UserSearchRole);
            DBManager.ImportInCombo(CurrentForm.form.UserSearchAgency, AgencyDetailsForm.TABLE_NAME, AgencyDetailsForm.COL_NAME, AgencyDetailsForm.COL_ID);
            DBManager.ImportInCombo(CurrentForm.form.RealSearchAgency, AgencyDetailsForm.TABLE_NAME, AgencyDetailsForm.COL_NAME, AgencyDetailsForm.COL_ID);
            DBManager.ImportInCombo(CurrentForm.form.RealSearchAgent, UserDetailsForm.TABLE_NAME, UserDetailsForm.COL_USERNAME, UserDetailsForm.COL_ID);
        }

        private System.Windows.Forms.ToolStripMenuItem editToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem languageToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem englishToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem serbianToolStripMenuItem;
        public Button RealestateCreate;

    }

}