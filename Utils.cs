﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PKIprojekat
{
    class UserType
    {

        public const string USER_TYPE_SYS_ADMIN = "sys_admin";
        public const string USER_TYPE_MODERATOR = "moderator";
        public const string USER_TYPE_AGENT = "agent";

        public const string USER_TYPE_NAME_SYS_ADMIN = "sys_admin";
        public const string USER_TYPE_NAME_MODERATOR = "moderator";
        public const string USER_TYPE_NAME_AGENT = "agent";

        public UserType(string code, string value)
        {
            _code = code;
            _value = value;
        }

        private string _code = null;
        private string _value = null;

        public string Code
        {
            get { return _code; }
            set { _code = value; }
        }
        public string Value
        {
            get { return _value; }
            set { _value = value; }
        }

        public static List<UserType> getUserTypes()
        {
            List<UserType> list = new List<UserType>();
            list.Add(new UserType(UserType.USER_TYPE_AGENT, UserType.USER_TYPE_NAME_AGENT));
            list.Add(new UserType(UserType.USER_TYPE_MODERATOR, UserType.USER_TYPE_NAME_MODERATOR));
            list.Add(new UserType(UserType.USER_TYPE_SYS_ADMIN, UserType.USER_TYPE_NAME_SYS_ADMIN));
            return list;
        }
    }

    
}
