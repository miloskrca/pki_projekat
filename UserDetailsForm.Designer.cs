﻿namespace PKIprojekat
{
    partial class UserDetailsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.UserDataAgency = new System.Windows.Forms.ComboBox();
            this.tblagencyBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.UserDataLastname = new System.Windows.Forms.TextBox();
            this.UserDataName = new System.Windows.Forms.TextBox();
            this.UserDataUsername = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.UserDataRole = new System.Windows.Forms.ComboBox();
            this.tblroleBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.roles = new PKIprojekat.Roles();
            this.tblroleBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.databaseDataSetBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.tbl_roleTableAdapter = new PKIprojekat.RolesTableAdapters.tbl_roleTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.tblagencyBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblroleBindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.roles)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblroleBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.databaseDataSetBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // UserDataAgency
            // 
            this.UserDataAgency.DataSource = this.tblagencyBindingSource;
            this.UserDataAgency.DisplayMember = "name";
            this.UserDataAgency.FormattingEnabled = true;
            this.UserDataAgency.Location = new System.Drawing.Point(249, 209);
            this.UserDataAgency.Name = "UserDataAgency";
            this.UserDataAgency.Size = new System.Drawing.Size(202, 21);
            this.UserDataAgency.TabIndex = 20;
            this.UserDataAgency.ValueMember = "id_agency";
            // 
            // UserDataLastname
            // 
            this.UserDataLastname.Location = new System.Drawing.Point(249, 183);
            this.UserDataLastname.Name = "UserDataLastname";
            this.UserDataLastname.Size = new System.Drawing.Size(203, 20);
            this.UserDataLastname.TabIndex = 19;
            // 
            // UserDataName
            // 
            this.UserDataName.Location = new System.Drawing.Point(249, 157);
            this.UserDataName.Name = "UserDataName";
            this.UserDataName.Size = new System.Drawing.Size(203, 20);
            this.UserDataName.TabIndex = 18;
            // 
            // UserDataUsername
            // 
            this.UserDataUsername.Enabled = false;
            this.UserDataUsername.Location = new System.Drawing.Point(247, 104);
            this.UserDataUsername.Name = "UserDataUsername";
            this.UserDataUsername.Size = new System.Drawing.Size(203, 20);
            this.UserDataUsername.TabIndex = 16;
            // 
            // label5
            // 
            this.label5.Location = new System.Drawing.Point(181, 212);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(60, 13);
            this.label5.TabIndex = 15;
            this.label5.Text = "Agencija:";
            this.label5.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label4
            // 
            this.label4.Location = new System.Drawing.Point(181, 186);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(60, 13);
            this.label4.TabIndex = 14;
            this.label4.Text = "Prezime:";
            this.label4.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(181, 160);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(60, 13);
            this.label3.TabIndex = 13;
            this.label3.Text = "Ime:";
            this.label3.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(182, 133);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(60, 13);
            this.label2.TabIndex = 12;
            this.label2.Text = "Uloga:";
            this.label2.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(161, 107);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(80, 13);
            this.label1.TabIndex = 11;
            this.label1.Text = "Korisnicko Ime:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // UserDataRole
            // 
            this.UserDataRole.DataSource = this.tblroleBindingSource1;
            this.UserDataRole.DisplayMember = "name";
            this.UserDataRole.FormattingEnabled = true;
            this.UserDataRole.Location = new System.Drawing.Point(249, 130);
            this.UserDataRole.Name = "UserDataRole";
            this.UserDataRole.Size = new System.Drawing.Size(202, 21);
            this.UserDataRole.TabIndex = 21;
            this.UserDataRole.ValueMember = "id_role";
            // 
            // tblroleBindingSource1
            // 
            this.tblroleBindingSource1.DataMember = "tbl_role";
            this.tblroleBindingSource1.DataSource = this.roles;
            // 
            // roles
            // 
            this.roles.DataSetName = "Roles";
            this.roles.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // tbl_roleTableAdapter
            // 
            this.tbl_roleTableAdapter.ClearBeforeFill = true;
            // 
            // UserDetailsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(617, 361);
            this.Controls.Add(this.UserDataRole);
            this.Controls.Add(this.UserDataAgency);
            this.Controls.Add(this.UserDataLastname);
            this.Controls.Add(this.UserDataName);
            this.Controls.Add(this.UserDataUsername);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "UserDetailsForm";
            this.Text = "UserDetailsForm";
            this.Load += new System.EventHandler(this.UserDetailsForm_Load);
            this.Controls.SetChildIndex(this.SaveDetailsForm, 0);
            this.Controls.SetChildIndex(this.CancelDetailsForm, 0);
            this.Controls.SetChildIndex(this.DeleteDetailsForm, 0);
            this.Controls.SetChildIndex(this.label1, 0);
            this.Controls.SetChildIndex(this.label2, 0);
            this.Controls.SetChildIndex(this.label3, 0);
            this.Controls.SetChildIndex(this.label4, 0);
            this.Controls.SetChildIndex(this.label5, 0);
            this.Controls.SetChildIndex(this.UserDataUsername, 0);
            this.Controls.SetChildIndex(this.UserDataName, 0);
            this.Controls.SetChildIndex(this.UserDataLastname, 0);
            this.Controls.SetChildIndex(this.UserDataAgency, 0);
            this.Controls.SetChildIndex(this.UserDataRole, 0);
            ((System.ComponentModel.ISupportInitialize)(this.tblagencyBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblroleBindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.roles)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblroleBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.databaseDataSetBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox UserDataAgency;
        private System.Windows.Forms.TextBox UserDataLastname;
        private System.Windows.Forms.TextBox UserDataName;
        private System.Windows.Forms.TextBox UserDataUsername;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox UserDataRole;

        private string _id = null;
        private string _username = null;
        private string _firstname = null;
        private string _lastname = null;
        private int _role = 0;

        public string Id
        {
            get { return _id; }
            set { _id = value; }
        }

        public string Username 
        {
            get { _username = UserDataUsername.Text; return _username;  }
            set { _username = value; UserDataUsername.Text = value; }
        }

        public string Firstname
        {
            get { _firstname = UserDataName.Text; return _firstname; }
            set { _firstname = value; UserDataName.Text = value; }
        }

        public string Lastname
        {
            get { _lastname = UserDataLastname.Text; return _lastname; }
            set { _lastname = value; UserDataLastname.Text = value; }
        }

        public int Role
        {
            get { _role = (int)UserDataRole.SelectedValue; return _role; }
            set { _role = value; UserDataRole.SelectedValue = value; UserDataRole.Refresh(); }
        }

        public int Agency
        {
            get
            {
                if (UserDataAgency.SelectedValue != null)
                    return (int)UserDataAgency.SelectedValue;
                else
                    return -1;
            }
            set { UserDataAgency.SelectedValue = value; UserDataAgency.Refresh(); }
        }

        public void EnableUsername(bool b)
        {
            UserDataUsername.Enabled = b;
        }

        public void reloadAgencies()
        {
            DBManager.ImportInCombo(UserDataAgency, AgencyDetailsForm.TABLE_NAME, AgencyDetailsForm.COL_NAME, AgencyDetailsForm.COL_ID);
        }

        private System.Windows.Forms.BindingSource databaseDataSetBindingSource;
        private System.Windows.Forms.BindingSource tblagencyBindingSource;
        private System.Windows.Forms.BindingSource tblroleBindingSource;
        private Roles roles;
        private System.Windows.Forms.BindingSource tblroleBindingSource1;
        private RolesTableAdapters.tbl_roleTableAdapter tbl_roleTableAdapter;

        public void setLanguage()
        {
            this.label1.Text = CurrentForm.form.ResourceManager.GetString("username");
            this.label2.Text = CurrentForm.form.ResourceManager.GetString("role");
            this.label3.Text = CurrentForm.form.ResourceManager.GetString("name");
            this.label4.Text = CurrentForm.form.ResourceManager.GetString("lastname");
            this.label5.Text = CurrentForm.form.ResourceManager.GetString("agency");
            this.DeleteDetailsForm.Text = CurrentForm.form.ResourceManager.GetString("delete");
            this.SaveDetailsForm.Text = CurrentForm.form.ResourceManager.GetString("save");
            this.CancelDetailsForm.Text = CurrentForm.form.ResourceManager.GetString("cancel");
            this.Text = CurrentForm.form.ResourceManager.GetString("user_details");


            setConstants();
        }

        public void setConstants()
        {
            DBManager.setRole(UserDataRole);
            DBManager.ImportInCombo(UserDataAgency, AgencyDetailsForm.TABLE_NAME, AgencyDetailsForm.COL_NAME, AgencyDetailsForm.COL_ID);
        }
    }
}