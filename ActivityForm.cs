﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections;

namespace PKIprojekat
{
    public partial class ActivityForm : Form
    {
        public int realeastate;
        public RealestateDetailsForm form;
        public bool same;
        public string id;
        public bool edit;

        public ActivityForm()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Activity a = new Activity(
                realeastate,
                this.dateTimePicker1.Text, 
                (int)this.comboBox1.SelectedValue,
                this.textBox1.Text,
                this.richTextBox1.Text);

            if (edit)
            {
                Hashtable hash = new Hashtable();
                hash.Add("date", a.date);
                hash.Add("client", a.client);
                hash.Add("remark", a.remark);
                DBManager.executeQueryUpdate("tbl_activity", hash, "id", id);
            }
            else
            {
                DBManager.insertActivity(a);
            }
            form.Activities.DataSource = null;
            form.Activities.DataSource = DBManager.getActivities(realeastate, same);
            form.Activities.AllowUserToAddRows = false;
            form.Activities.ReadOnly = true;
            form.Activities.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            this.Dispose();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            DBManager.executeQueryDelete("tbl_activity", "id", id);
            form.Activities.DataSource = DBManager.getActivities(realeastate, same);
            form.Activities.AllowUserToAddRows = false;
            form.Activities.ReadOnly = true;
            form.Activities.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            this.Dispose();
        }
    }
}
